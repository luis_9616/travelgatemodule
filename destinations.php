<?php
require_once ('model/database.php');//fluentPDO
$controller = 'mapping';
require_once "controller/$controller.controller.php";
$controller = ucwords($controller).'controller';
$controller = new $controller;

if (isset($_POST['accessKey']) && isset($_POST['range'])) {
  $tokenDestinations = '';
  $sizeDestinations = $_POST['range']; 
  $accessSupplier = $_POST['accessKey'];
  $id_sup = $_POST['id_sup'];
  $controller->destinations($tokenDestinations, $sizeDestinations, $accessSupplier,$id_sup);
}else {
    echo('No puedes acceder directamente!');
    header('location: http://travelgatemodule.u/');
}
