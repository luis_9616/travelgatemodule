<?php
//call library of Graphql/client for mostafa
 require_once __DIR__ . '/../vendor/autoload.php';
 // use GraphQL\Client;
 use GraphQL\Exception\QueryError;
 use GraphQL\Client;
 use GraphQL\Query;

    class Travelgate{
      // variables for all the functions
      // variables for all the functions
      // variables for all the functions
        private $pdo;
        private $apikey;
        public $checkIn;
        public $checkOut;
        public $rooms;
        public $ClientName;
        public $Context;
        public $language;
        public $nationality;
        public $currency;
        public $market;
        public $hotels;
        public $context;
        public $tokenDestinations;
        public $sizeDestinations;
        public $accessSupplier;

        // ================================
        // constructor for the bd conection
        // ================================
        public function __CONSTRUCT(){
            try {
                $this->pdo = Database::StartUp();
            } catch (\Throwable $th) {
               die($th->getMessage());
            }
        }

        // ======================================
        // función para buscar los datos en la bd
        // ======================================
        public function Credential($id){
            try {
                $cdt = $this->pdo
                    ->prepare('SELECT * FROM travelconfig WHERE id = ?');
                $cdt->execute(array($id));

                return $cdt->fetch(PDO::FETCH_OBJ);
            } catch (\Throwable $th) {
                echo('DB conect error: ');
                die($th->getMessage());

            }
        }

        // ==========================================================================================
        // función para generar el cliente que hace toda las consultas a travelgate desde el servidor
        // ==========================================================================================
        public function getClient(){
            try {
            $datas = $this->Credential(1);
            $apikey = $datas->apikey;
            $travelgateurl = $datas->travelgateurl;
            // Create Client object to contact the GraphQL endpoint
            $client = new  Client(
                $travelgateurl,
                ["authorization"=>"ApiKey ".$apikey]
            );
            return $client;
            } catch (\Throwable $th) {
              echo('Get client error: ');
                die($th->getMessage());
            }
        }

        // ==========================================================
        // funcion para hacer las peticiones de busqueda a travelgate
        // ==========================================================
        public function makeSearch($datas){
          $client = $this ->getClient();
          // var_dump($client); // var_dump($datas);
          $credentials = $this->Credential(1);
          // var_dump($credentials);
            if($client != null && $datas != null) {
                // var_dump($client); // var_dump($datas);

                $checkIn = $datas[0];
                $checkOut = $datas[1];
                $rooms = $datas[2];

                $ClientName = $credentials->clientName;
                $context = 'HOTELTEST';
                $array =  array();
                for ($i=1; $i <= $rooms; $i++) {
                    array_push($array,"{paxes:[{age:23},{age:11}]},");
                }
                $paxes = implode($array);//une los elementos de array en un string

                $querySearch = 'query searches{
                    hotelX {
                      search(
                        criteria:{
                          checkIn:"'.$checkIn.'"
                          checkOut:"'.$checkOut.'"
                            hotels:["2"]
                          
                          occupancies:
                            [
                             '.$paxes.'
                            ]
                          nationality:"US"
                          currency:"USD"
                          destinations: ["1149", "US", "31256"]
                          market:"US"
                        },
                        settings:{
                          businessRules: null
                          timeout:24700
                          client: "'.$ClientName.'"
                          context:"'.$context.'"
                          auditTransactions:true
                          testMode:true
                        },
                        filter:{
                          access:{
                            includes:[
                             "881","0"
                              
                            ],
                            excludes:null
                          }
                        
                        }
                      ) {
                        #auditData {
                          #transactions {
                            #request
                           # response
                          #}
                        #}
                        context
                        options {
                          surcharges {
                            chargeType
                            mandatory
                            description
                            price {
                              currency
                              binding
                              net
                              gross
                              exchange {
                                currency
                                rate
                              }
                              markups {
                                channel
                                currency
                                binding
                                net
                                gross
                                exchange {
                                  currency
                                  rate
                                }
                              }
                            }
                          }
                          accessCode
                          supplierCode
                          market
                          hotelCode
                          hotelName
                          boardCode
                          paymentType
                          status
                          occupancies {
                            id
                            paxes {
                              age
                            }
                          }
                          
                          rooms {
                            occupancyRefId
                            code
                            description
                            refundable
                            units
                            roomPrice {
                              price {
                                currency
                                binding
                                net
                                gross
                                exchange {
                                  currency
                                  rate
                                }
                                markups {
                                  channel
                                  currency
                                  binding
                                  net
                                  gross
                                  exchange {
                                    currency
                                    rate
                                  }
                                }
                              }
                            }
                            beds {
                              type
                              description
                              count
                              shared
                            }
                            ratePlans {
                              code
                              name
                              effectiveDate
                              expireDate
                            }
                            promotions {
                              code
                              name
                              effectiveDate
                              expireDate
                            }
                          }
                          price {
                            currency
                            binding
                            net
                            gross
                            exchange {
                              currency
                              rate
                            }
                            markups {
                              channel
                              currency
                              binding
                              net
                              gross
                              exchange {
                                currency
                                rate
                              }
                            }
                          }
                          
                          supplements {
                            code
                            name
                            description
                            supplementType
                            chargeType
                            mandatory
                            durationType
                            quantity
                            unit
                            effectiveDate
                            expireDate
                            resort {
                              code
                              name
                              description
                            }
                            price {
                              currency
                              binding
                              net
                              gross
                              exchange {
                                currency
                                rate
                              }
                              markups {
                                channel
                                currency
                                binding
                                net
                                gross
                                exchange {
                                  currency
                                  rate
                                }
                              }
                            }
                          }
                          surcharges {
                            chargeType
                            description
                            price {
                              currency
                              binding
                              net
                              gross
                              exchange {
                                currency
                                rate
                              }
                              markups {
                                channel
                                currency
                                binding
                                net
                                gross
                                exchange {
                                  currency
                                  rate
                                }
                              }
                            }
                          }
                          rateRules
                          cancelPolicy {
                            refundable
                            cancelPenalties {
                              hoursBefore
                              penaltyType
                              currency
                              value
                            }
                          }
                          remarks
                          token
                          id
                        }
                        errors {
                          code
                          type
                          description
                        }
                        warnings {
                          code
                          type
                          description
                        }
                      }
                    }
                  }';                
                
$gql = <<<QUERY
$querySearch
QUERY;
                try {
                    $result = $client->runRawQuery($gql);
                    $resultsSearches = $result->getData()->hotelX;
                } catch (QueryError $exception) {
                    print_r($exception->getErrorDetails());
                    exit;
                }
                return $resultsSearches;
                }else{
                print_r('Error on the function');
            }
        }
        
        // ======================================================================================================================
        // funcion para tomar una de las opciones de los resultados y generar un token el cual sirva para realizar la reservacion
        // ======================================================================================================================
        public function makeQuote($datas){
          $client = $this ->getClient();
          $credentials = $this->Credential(1);
          if ($datas['token'] != null || "") {
$queryQuote = 'query quote{
            hotelX{
              quote(
                criteria:{
                  optionRefId:"'.$datas['token'].'"
                },
                settings:{
                  client: "'.$credentials->clientName.'"
                  testMode:false
                  context:"'.$datas['context'].'"
                },
                
              ){
                optionQuote{
                  optionRefId
                  status
                  price{
                    currency
                    net
                    gross
                  }
                  surcharges{
                    description
                    price{
                      currency
                      net
                      gross
                      binding
                      markups{
                        channel
                      }
                    }
                  }
                  cancelPolicy{
                    refundable
                  }
                  remarks
                }
                errors{
                  code
                  type
                  description
                }
                warnings{
                  code
                  type
                  description
                }
              }
            }
          }
        ';
        // Create the GraphQL query
$gql = <<<QUERY
$queryQuote
QUERY;

        try {
          $results = $client->runRawQuery($gql);
        }
        catch (QueryError $exception) {
          print_r($exception->getErrorDetails());
          exit;
        }

        $quoteRes = $results->getData()->hotelX;
        return $quoteRes;
          }else {
            print_r('Error token not found');
          }
        }

        // ==========================================================
        // funcion para realizar la reservacion con el token obtenedo
        // ==========================================================
        public function makeBook($token){
          if ($token != null || ""){
            //query
          }else {
            print_r('token not found');
          }
        }

        // =============================================
        // funcion para traer los destinos por proveedor
        // =============================================
        public function getDestinations($tokenDestinations, $sizeDestinations, $accessSupplier){
          if ($sizeDestinations != "" && $accessSupplier != "") {
            $client = $this ->getClient();
 $queryText = 'query fliterSearch{
    hotelX {
      destinations(
        token:"'.$tokenDestinations.'"
    criteria: {
      maxSize: '.$sizeDestinations.'
      access: "'.$accessSupplier.'",
    },
    relay:{}
  ) {
        token
        edges {
          cursor
          node {
            code
            destinationData {
              code
              available
              destinationLeaf
              closestDestinations
              parent
              type
              texts {
                language
                text
              }
            }
            
            error{
              code
              type
              description
            }
            createdAt
            updatedAt
          }
        }
      }
    }
  }';
// Create the GraphQL query
$gql = <<<QUERY
$queryText
QUERY;

try {
    $results = $client->runRawQuery($gql);
    $destinations = $results->getData()->hotelX;

    return $destinations;
}
catch (QueryError $exception) {
    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}
    }else {
      print_r('error');
    }
  }


  // ==========================================================================================
  // funcion para traer los datos de los hoteles // funcion para traer los datos de los hoteles
  // ==========================================================================================  
  public function getDataHotels($access, $hotelCode){
    # Search by hotel codes.
    # to get all static data about the hotelCode
          if ($access != "" && $hotelCode != "") {
            $client = $this ->getClient();
 $queryText = 'query luisRules2{
   hotelX {
     hotels(criteria: {
     access: "'.$access.'",
     hotelCodes: ["'.$hotelCode.'"]
   },
   relay: {}) {
       token
       edges {
         node {
           code
           hotelData {
             code
             hotelCode
             hotelCodeSupplier
             hotelName
             categoryCode
             rank
             cardTypes
             contact {
               email
               telephone
               fax
               web
             }
             property{
               name
               code
             }
             chainCode
             exclusiveDeal
             rank
             cardTypes
             location {
               address
               city
               zipCode
               country
               coordinates {
                 latitude
                 longitude
               }
               closestDestination {
                 code
                 available
                 destinationLeaf
                 parent
                 type
                 texts {
                   text
                   language
                 }
               }
             }
             amenities {
               code
               type
               texts {
                 text
                 language
               }
             }
             medias {
               code
               order
               type
               updatedAt
               url
               texts {
                 text
                 language
               }
             }
             descriptions {
               type
               texts {
                 text
                 language
               }
             }
           }
           error {
             code
             type
             description
           }
           createdAt
           updatedAt
         }
       }
     }
   }
 }';
// Create the GraphQL query
$gql = <<<QUERY
$queryText
QUERY;

try {
    $results = $client->runRawQuery($gql);
    $hotelDatas = $results->getData()->hotelX;

    return $hotelDatas;
}
// Catch query error and desplay error details
catch (QueryError $exception) {
    print_r($exception->getErrorDetails());
    exit;
}
    }else {
      print_r('error...');
    }
  }

  // ================================================================
  // query para traer todos los hoteles segun el destino seleccionado
  // ================================================================  
  public function getAllHotelByDestiny($datas, $settingCurrency, $codes){
    // creamos el cliente graph para la consulta a la API
    $client = $this ->getClient();

    // instanciamos un objeto con los datos de perseus
    $credentials = $this->Credential(1);   
    // detalles del cliente (perseus)
    $clientName = $credentials->clientName;

    // ==============================
    // datos generales de la busqueda
    // ==============================
    if($client != null && $datas != null) {
      $checkIn = $datas['checkIn'];
      $checkOut = $datas['checkOut'];
      // numero de habitaciones
      // !!!!!!falta meterle el numero de personas por habitacion!!!!!!!
      $rooms = $datas['rooms'];


      // ======================
      // detalles del proveedor
      // ======================
      $tmp = json_decode($datas['datSupplier']);
      // codigo asignado al proveedor
      $code = $tmp->sup_code;
      // algo
      $context = $tmp->sup_code;
      // codigo de acceso del proveedor
      $value = $tmp->sup_access;

      // ==========================================================
      // configuracion para el idioma en el que se hace la busqueda
      // ==========================================================
      $language = $settingCurrency['lang'];
      $nationality = $settingCurrency['nationality'];
      $currency = $settingCurrency['currency'];
      $market = $settingCurrency['market'];
      
      // =============================================
      // manipular el array con los codigos de destino
      // =============================================
      $flag = 0;
      if (!empty($codes['leaf'])){
        $auxLeaf = json_encode($codes['leaf']);
          // $auxLeaf = implode('","',$codes['leaf']);
          // $auxLeaf = '"'.$auxLeaf.'"';
          // echo "<br>";
          // echo $auxLeaf;
          $flag = 1;
      }else{
        $auxClose = json_encode($codes['closes']);
        // $auxClose = '"'.$auxClose.'"';
            // echo "<br>";
            // echo $auxClose;
      }

      if ($flag == 1) {
          $busca = $auxLeaf;
      }else{
        $busca = $auxClose;
      }

       // $comma_separated = implode("','", $array); 
       // $comma_separated = "'".$comma_separated."'"; 
       // echo $comma_separated;


      $array =  array();
      for ($i=1; $i <= $rooms; $i++) {
          array_push($array,"{paxes:[{age:23},{age:21}]},");
      }
      $paxes = implode($array);//une los elementos de array en un string

      // =============================================================================
      // query que hace toda la magia de traer los hotels disponibles para la busqueda
      // =============================================================================
      $querySearch = 'query luisRules{
        hotelX {
          search(criteria: {
            checkIn: "'.$checkIn.'", checkOut: "'.$checkOut.'", 
            occupancies:
            ['.$paxes.'] 
            language: "'.$language.'",	
            nationality: "'.$nationality.'",
            currency: "'.$currency.'",
            market: "'.$market.'",
          destinations:'.$busca.'
          }, 
            settings: {
              suppliers: {
                code: "'.$code.'"
              },
          plugins: {
            step: REQUEST, 
            pluginsType: [
              {
                type: POST_STEP, 
                name: "search_by_destination", 
          parameters: [
            {
              key: "accessID", 
              value: "'.$value.'"
            }
          ]
              }
            ]
          }, 
              businessRules: null, 
              timeout: 24700, 
          context: "HDO", 
              client: "'.$clientName.'", 
              testMode: false}, 
            filter: {
              access: {
                includes: ["'.$value.'"]
              }
            }
          ) {
            context
            warnings {
              code
              type
              description
            }
            options {
              surcharges {
                chargeType
                mandatory
                description
                price {
                  currency
                  binding
                  net
                  gross
                  exchange {
                    currency
                    rate
                  }
                  markups {
                    channel
                    currency
                    binding
                    net
                    gross
                    exchange {
                      currency
                      rate
                    }
                  }
                }
              }
              accessCode
              supplierCode
              market
              hotelCode
              hotelName
              boardCode
              paymentType
              status
              occupancies {
                id
                paxes {
                  age
                }
              }
              rooms {
                features{
                   code
                }
                supplierCode
                occupancyRefId
                code
                description
                refundable
                units
                roomPrice {
                  price {
                    currency
                    binding
                    net
                    gross
                    exchange {
                      currency
                      rate
                    }
                    markups {
                      channel
                      currency
                      binding
                      net
                      gross
                      exchange {
                        currency
                        rate
                      }
                    }
                  }
                }
                beds {
                  type
                  description
                  count
                  shared
                }
                ratePlans {
                  code
                  name
                  effectiveDate
                  expireDate
                }
                promotions {
                  code
                  name
                  effectiveDate
                  expireDate
                }
              }
              price {
                currency
                binding
                net
                gross
                exchange {
                  currency
                  rate
                }
                markups {
                  channel
                  currency
                  binding
                  net
                  gross
                  exchange {
                    currency
                    rate
                  }
                }
              }
              supplements {
                code
                name
                description
                supplementType
                chargeType
                mandatory
                durationType
                quantity
                unit
                effectiveDate
                expireDate
                resort {
                  code
                  name
                  description
                }
                price {
                  currency
                  binding
                  net
                  gross
                  exchange {
                    currency
                    rate
                  }
                  markups {
                    channel
                    currency
                    binding
                    net
                    gross
                    exchange {
                      currency
                      rate
                    }
                  }
                }
              }
              surcharges {
                chargeType
                description
                price {
                  currency
                  binding
                  net
                  gross
                  exchange {
                    currency
                    rate
                  }
                  markups {
                    channel
                    currency
                    binding
                    gross
                    exchange {
                      currency
                      rate
                    }
                  }
                }
              }
              rateRules
              cancelPolicy {
                refundable
                cancelPenalties {
                  hoursBefore
                  penaltyType
                  currency
                  value
                }
              }
              remarks
              token
              id
            }
            errors {
              code
              type
              description
            }
            warnings {
              code
              type
              description
            }
          }
        }
      }';                
    
$gql = <<<QUERY
$querySearch
QUERY;

      try {
          $result = $client->runRawQuery($gql);
          $resultsSearches = $result->getData()->hotelX;
      } catch (QueryError $exception) {
          print_r($exception->getErrorDetails());
          exit;
      }
      return $resultsSearches;
      }else{
      print_r('Error on the function');
  }
  }

  // ============================================================
  // Query para traer todas la habitaciones disponibles por hotel
  // ============================================================
  public function getOnlyRoomsByHotel($datas, $settingCurrency, $hotelCode){
    // creamos el cliente graph para la consulta a la API
    $client = $this ->getClient();
        	
    // instanciamos un objeto con los datos de perseus
    $credentials = $this->Credential(1);   

    // detalles del cliente (perseus)
    $clientName = $credentials->clientName;

    // ==============================
    // datos generales de la busqueda
    // ==============================
    if($client != null && $datas != null) {
      $checkIn = $datas->checkIn;
      $checkOut = $datas->checkOut;
      // numero de habitaciones // !!!!!!falta meterle el numero de personas por habitacion!!!!!!!
      $rooms = $datas->rooms;

      // ======================
      // detalles del proveedor
      // ======================
      $tmp = json_decode($datas->datSupplier);
      // codigo asignado al proveedor
      $code = $tmp->sup_code;
      // algo
      $context = $tmp->sup_code;
      // codigo de acceso del proveedor
      $value = $tmp->sup_access;

      // ==========================================================
      // configuracion para el idioma en el que se hace la busqueda
      // ==========================================================
      $language = $settingCurrency['lang'];
      $nationality = $settingCurrency['nationality'];
      $currency = $settingCurrency['currency'];
      $market = $settingCurrency['market'];

      $array =  array();
      for ($i=1; $i <= $rooms; $i++) {
          array_push($array,"{paxes:[{age:23},{age:21}]},");
      }
      $paxes = implode($array);//une los elementos de array en un string

      // =============================================================================
      // query que hace toda la magia de traer los hotels disponibles para la busqueda
      // =============================================================================
      $querySearch = 'query luisRules{
        hotelX {
          search(
            criteria:{
              checkIn:"'.$checkIn.'"
              checkOut:"'.$checkOut.'"
              hotels:["'.$hotelCode.'"]
              occupancies:
            ['.$paxes.'] 
            language: "'.$language.'",	
            nationality: "'.$nationality.'",
            currency: "'.$currency.'",
            market: "'.$market.'",
            },
            settings:{
              businessRules: null
              timeout:24700
              client: "'.$clientName.'"
              context:"HDO"
              auditTransactions:false
              testMode:false
            },
            filter:{
              access:{
                includes:[
                  "'.$value.'"
                ],
                excludes:null
              }
            }
          ) {
            auditData {
              transactions {
                request
                response
              }
            }
            context
            
            options {
              
              surcharges {
                chargeType
                mandatory
                description
                price {
                  currency
                  binding
                  net
                  gross
                  
                  exchange {
                    currency
                    
                    rate
                  }
                  markups {
                    channel
                    currency
                    binding
                    net
                    gross
                    exchange {
                      currency
                      rate
                    }
                  }
                }
              }
              accessCode
              supplierCode
              market
              hotelCode
              hotelName
              boardCode
              paymentType
              status
              occupancies {
                id
                paxes {
                  age
                }
              }
              
              rooms {
                occupancyRefId
                code
                description
                refundable
                units
                features{
                  code
                  
                }
                roomPrice {
                  price {
                    currency
                    binding
                    net
                    gross
                    exchange {
                      currency
                      rate
                    }
                    markups {
                      channel
                      currency
                      binding
                      net
                      gross
                      exchange {
                        currency
                        rate
                      }
                    }
                  }
                }
                beds {
                  type
                  description
                  count
                  shared
                }
                ratePlans {
                  code
                  name
                  effectiveDate
                  expireDate
                }
                promotions {
                  code
                  name
                  effectiveDate
                  expireDate
                }
              }
              price {
                currency
                binding
                net
                gross
                exchange {
                  currency
                  rate
                }
                markups {
                  channel
                  currency
                  binding
                  net
                  gross
                  exchange {
                    currency
                    rate
                  }
                }
              }
              
              supplements {
                code
                name
                description
                supplementType
                chargeType
                mandatory
                durationType
                quantity
                unit
                effectiveDate
                expireDate
                resort {
                  code
                  name
                  description
                }
                price {
                  currency
                  binding
                  net
                  gross
                  exchange {
                    currency
                    rate
                  }
                  markups {
                    channel
                    currency
                    binding
                    net
                    gross
                    exchange {
                      currency
                      rate
                    }
                  }
                }
              }
              surcharges {
                chargeType
                description
                price {
                  currency
                  binding
                  net
                  gross
                  exchange {
                    currency
                    rate
                  }
                  markups {
                    channel
                    currency
                    binding
                    net
                    gross
                    exchange {
                      currency
                      rate
                    }
                  }
                }
              }
              rateRules
              cancelPolicy {
                refundable
                cancelPenalties {
                  hoursBefore
                  penaltyType
                  currency
                  value
                }
              }
              remarks
              token
              id
            }
            errors {
              code
              type
              description
            }
            warnings {
              code
              type
              description
            }
          }
        }
      }';                
    
$gql = <<<QUERY
$querySearch
QUERY;

      try {
          $result = $client->runRawQuery($gql);
          $resultsSearches = $result->getData()->hotelX;
      } catch (QueryError $exception) {
          print_r($exception->getErrorDetails());
          exit;
      }
      return $resultsSearches;
      }else{
      print_r('Error on the function');
  }
  }

}
