<?php
    $addStyle = '<link rel="stylesheet" href="../src/css/quote.css"> <script src="../src/leaflet/leaflet.js"></script><link rel="stylesheet" href="../src/leaflet/leaflet.css">';
    include 'header.php';
    $stars = $mediaHotel->hotels->edges[0]->node->hotelData->categoryCode;
    $amenities = $mediaHotel->hotels->edges[0]->node->hotelData->amenities;
?>
<section class="container">
    <div class="row">    
        <article class="card w-100 sticky-top fixed-top">
            <div class="card-header d-flex flex-row flex-wrap justify-content-between">
                <div id="hName">
                
                <?php // ================
                      // Nombre del hotel 
                      // ================ 
                ?>
                    <h3 style="color:#2da44a;font-weight: bold !important;"> <?php echo $searches->search->options[0]->hotelName ?></h3>
                    <?php

                    // ===================
                    // Categoria del hotel
                    // ===================
                        if ($stars == 'S1') {
                            echo str_repeat('<i class="fas fa-star"></i>',1);
                           }elseif ($stars == 'S15') {
                            echo str_repeat('<i class="fas fa-star"></i>',1) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S2') {
                            echo str_repeat('<i class="fas fa-star"></i>',2);
                           }elseif ($stars == 'S25') {
                            echo str_repeat('<i class="fas fa-star"></i>',2) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S3') {
                            echo str_repeat('<i class="fas fa-star"></i>',3);
                           }elseif ($stars == 'S35') {
                            echo str_repeat('<i class="fas fa-star"></i>',3) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S4') {
                            echo str_repeat('<i class="fas fa-star"></i>',4);
                           }elseif ($stars == 'S45') {
                            echo str_repeat('<i class="fas fa-star"></i>',4) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S5') {
                            echo str_repeat('<i class="fas fa-star"></i>',5);
                           }else {
                               echo $stars;
                           }
                        ?>
                </div>
                <div id="goBtn" class="d-flex justify-content-center align-items-center">
                    <a href="#listRooms" class="btn btn-success" style="color:#fff">Reservar ahora</a>
                </div>
            </div>
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link active nav-link-fixed" href="#listRooms">Habitaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-fixed" href="#services">Servicios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-fixed" href="#politicies">Políticas</a>
                </li>
            </ul>
        </article>
        <article class="mediaDetail d-flex flex-row flex-wrap border mt-2">
            
            <!-- ======== -->
            <!-- carousel -->
            <!-- ======== -->
                <div id="carouselExampleCaptions" class="carousel slide carousel-fade col-sm-12 col-md-12 col-lg-7" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php
                    // for para
                        for($i=0; $i < count($mediaHotel->hotels->edges[0]->node->hotelData->medias); $i++) {
                        if ($i == 0) {
                            ?>
                            <li data-target="#carouselSelected" data-slide-to="<?php echo $i?>" class="active"></li>
                        <?php 
                            }else {
                        ?>
                            <li data-target="#carouselSelected" data-slide-to="<?php echo $i?>"></li>
                        <?php 
                            }
                        }
                        ?>
                    </ol>
                    <div class="carousel-inner"><!-- mi -->                    
                    <?php
                // for para las imagenes
                        for($i=0; $i < count($mediaHotel->hotels->edges[0]->node->hotelData->medias); $i++) {
                            $img = $mediaHotel->hotels->edges[0]->node->hotelData->medias;
                            $textEs;
                            for ($z=0; $z < count($img[$i]->texts) ; $z++) {
                                if ($img[$i]->texts[$z]->language == 'es') {
                                    $textEs = $img[$i]->texts[$z]->text;
                                }
                            }
                            if ($i == 0) {
                            ?>
                                <div class="carousel-item active">
                                <img src="<?php echo $img[$i]->url?>" class="d-block w-100" alt="<?php echo $textEs ?>">
                                    <!-- <img src="../src/img/hotels-1.jpg" class="d-block w-100" alt=""> -->
                                    <div class="carousel-caption d-none d-md-block descriptionImg">
                                        <h5><?php echo $img[$i]->type ?></h5>
                                        <p><?php echo $textEs?></p>
                                    </div>
                                </div>
                            <?php
                            }else {
                            ?>
                                <div class="carousel-item">
                                <img src="<?php echo $img[$i]->url?>" class="d-block w-100" alt="<?php echo $textEs ?>">
                                    <!-- <img src="../src/img/hotels-2.jpg" class="d-block w-100" alt=""> -->
                                    <div class="carousel-caption d-none d-md-block descriptionImg">
                                        <h5><?php echo $img[$i]->type ?></h5>
                                        <p><?php echo $textEs ?></p>
                                    </div>
                                </div>
                            <?php
                        }
                    } // for para las imagenes
                ?>
            </div> <!-- fin inner carrusel -->
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
            </div>

            <!-- ========= -->
            <!-- Amenities -->
            <!-- ========= -->
            <div id="details" class="col-sm-12 col-md-12 col-lg-5 d-flex flex-column flex-wrap">
                <div class="services">
                <?php
                   
                   $contServices = 0;
                   foreach ($amenities as $service) {
                       $textServ;
                    for ($z=0; $z < count($service->texts) ; $z++) { 
                        if ($service->texts[$z]->language == 'es') {
                            $textServ = $service->texts[$z]->text;
                        }
                    }
                       ?>
                       <p><i class="fas fa-th-list amenities"></i> <?php echo $textServ?></p>
               <?php
                    if ($contServices == 3) {
                    break;
                    }
                    $contServices++;
                        }
                    ?>
                    <a href="#allServices" class="right float-right nav-link-fixed">Todos los servicios <i class="fas fa-hand-point-right"></i></a>
                </div>

                <!-- ============== -->
                <!-- mapa del lugar -->
                <!-- ============== -->
                <div id="map" class="map"></div>
                <a href="#" class="amenities"><i class="fas fa-map-marker-alt amenities"></i> <?php echo $mediaHotel->hotels->edges[0]->node->hotelData->location->address.' '.$mediaHotel->hotels->edges[0]->node->hotelData->location->city ?></a>
            </div>
            <p class="p-3" style="color:#333">
                    <?php
                    foreach ($mediaHotel->hotels->edges[0]->node->hotelData->descriptions as $description) {
                        if ($description->type == 'GENERAL') {
                            $desText;
                            for ($zz=0; $zz < count($description->texts) ; $zz++) { 
                                if ($description->texts[$zz]->language=='es') {
                                    $desText = $description->texts[$zz]->text;
                                }
                            }
                            echo($desText);
                        }
                    ?>
                    <?php
                    }
                    ?>
                </p>
        </article>
    </div>
</section>
<section class="container">
    <div class="row">
        <h4 class=" m-3">Elige la habitación</h4>
        <hr>
    </div>
</section>

<section id="divBox" class="container products-list products-list2 p-0">
    <div class="row m-0">
        <!-- ancla -->
        <a id="listRooms"></a>
                        
                <!-- ======== -->
                <!-- articulo -->
                <!-- ======== -->
    <?php
            $hcodes = array();
            $tRooms = array();
            $cont = 0;
            $flagH = 1;
            foreach ($searches->search->options as $option) {
                ${"tRooms".$flagH} = array();
    ?>
                <form action="../quote.php"
                      method="post" 
                      class="product-item product-item2 col-sm-12 col-md-12 col-lg-12" 
                      data-price="<?php  if ($option->price->currency == 'USD') {
                                echo number_format(($option->price->net*$tmp->usd), 2, '.', '');
                            }elseif ($option->price->currency == 'EUR') {
                                echo number_format(($option->price->net*$tmp->eur), 2, '.', '');
                            } ?>"
                      data-cateCode="<?php echo($stars) ?>" 
                      data-hotelName="<?php echo($option->hotelName)?>" 
                      category="">
                      <input type="hidden" name="access" value="<?php echo $option->accessCode?>">
                      <input type="hidden" name="context" value="<?php echo $option->supplierCode?>">
                    <!-- agregar los inputs con los datos a enviar a la siguiente pagina para continuar con la transaccion de la habitacion el query quote -->  
                    <!-- ========= -->
                    <!-- seccion 1 -->
                    <!-- ========= -->
                    <div id="carouselExampleControls<?php echo $option->hotelCode.$cont?>" 
                         class="carousel slide carouselExampleControls col-sm-12 col-md-12 col-lg-4 border p-0"
                         data-ride="carousel">
                        <div class="carousel-inner col-sm-12 col-md-12 col-lg-12">
                            <?php
                            for($i=0; $i < count($mediaHotel->hotels->edges[0]->node->hotelData->medias); $i++) {
                                $imgz = $mediaHotel->hotels->edges[0]->node->hotelData->medias;
                                if ($i == 0) {
                                 ?>
                                 <div class="carousel-item active">
                                    <img src="<?php echo ($imgz[$i]->url); ?>" class="d-block" alt="...">
                                </div>
                                <?php
                                }else {
                                    ?>
                                    <div class="carousel-item">
                                        <img src="<?php echo ($imgz[$i]->url); ?>" class="d-block" alt="...">
                                    </div>
                                    <?php                                    
                                }
                            }
                            ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls<?php echo $option->hotelCode.$cont?>" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls<?php echo $option->hotelCode.$cont?>" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <!-- ========= -->
                    <!-- seccion 2 -->
                    <!-- ========= -->
                    <div class="detail col-xs-12 col-sm-12 col-md-12 col-lg-3">
                        <h4 class="htl-name" ><?php echo $option->hotelName?></h4>
                        <input type="hidden" name="hName" value="<?php echo $option->hotelName?>">
                        <?php
                        if ($stars == 'S1') {
                            echo str_repeat('<i class="fas fa-star"></i>',1);
                           }elseif ($stars == 'S15') {
                            echo str_repeat('<i class="fas fa-star"></i>',1) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S2') {
                            echo str_repeat('<i class="fas fa-star"></i>',2);
                           }elseif ($stars == 'S25') {
                            echo str_repeat('<i class="fas fa-star"></i>',2) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S3') {
                            echo str_repeat('<i class="fas fa-star"></i>',3);
                           }elseif ($stars == 'S35') {
                            echo str_repeat('<i class="fas fa-star"></i>',3) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S4') {
                            echo str_repeat('<i class="fas fa-star"></i>',4);
                           }elseif ($stars == 'S45') {
                            echo str_repeat('<i class="fas fa-star"></i>',4) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S5') {
                            echo str_repeat('<i class="fas fa-star"></i>',5);
                           }else {
                               echo $stars;
                           }
                        ?>
                        <!-- <p class="border" style=""> -->
                        <?php
                        foreach ($option->rooms as $value) {
                            echo ('<p style="font-size:13px" class=" col-12">Código: '.$value->code.'<br> Habitación tipo: '.$value->description.'</p>');
                            array_push(${"tRooms".$flagH}, $value->description);
                        }
                        ?>
                        <!-- </p> -->
                    </div>
                    <!-- ========= -->
                    <!-- seccion 3 -->
                    <!-- ========= -->
                    <div class="detail fw col-xs-12 col-sm-12 col-md-12 col-lg-3  pl-1 pr-1">
                        <p class="" style=""><?php
                                if ($option->status == 'OK') {
                                    echo 'Disponible';
                                }else{
                                    echo 'No disponible';
                                }
                        ?></p>
                        <?php
                            foreach ($option->rooms as $prmos) {
                                if ($prmos->promotions !== null) {
                                    foreach ($prmos->promotions as $prmDtail) {
                                        if ($prmDtail->effectiveDate != null) {
                                            echo 'Fecha efectiva: '. $prmDtail->effectiveDate.'<br>';
                                        }
                                        if ($prmDtail->expireDate != null) {
                                            echo 'Fecha de caducidad: '.$prmDtail->expireDate.'<br>';
                                        }
                                        echo '<p style="font-size:11px" class=" col-12 pl-0">'.$prmDtail->name.'</p>';
                                    }
                                }
                            }
                            if($option->rateRules !== null){
                                foreach ($option->rateRules as $rates) {
                                        if ($rates == 'NON_REFUNDABLE') {
                                            echo('<p class="" style="color:red;"> No reembolsable!</p>');
                                        }else{
                                            echo($rates);
                                        }
                                }
                            }
                            echo '<p style="cursor:pointer;" tabindex="0" class="colorText2 " role="button" data-toggle="popover" data-trigger="focus" title="Observaciones del establecimiento" data-content="'.$option->remarks.'">Observaciones <i class="fas fa-exclamation colorText1"></i></p>';
                        ?>
                    </div>
                    <!-- ========= -->
                    <!-- seccion 4 -->
                    <!-- ========= -->
                    <div class="cash col-xs-12 col-sm-12 col-md-12 col-lg-2 border">
                        <div class="promo promo2">
                        <?php
                        $flag2 = 0;
                            foreach ($option->rooms as $prmos) {
                                if ($prmos->promotions !== null) {
                                    echo '<span class="text-promo">Con Promoción</span>';
                                    if ($flag2==0) {
                                    break;
                                    }
                                }else{
                                    echo '<p class="bg-info"></p>';
                                }
                                $flag2++;
                            }
                        ?>
                        </div>
                        <div class="real- mt-5 col-12">
                        <p style="font-size:12px">Precios por habitación</p>
                            <!-- <div class="old-price">$400 .<span class="currency">MXN</span></div> -->
                            <?php 
                            foreach ($option->rooms as $value) {
                                if ($value->roomPrice->price->currency == 'USD') {
                                    echo ('<p style="font-size:" class="col-12 new-price"> $'.number_format(($value->roomPrice->price->net*$tmp->usd), 2, '.', '').'<span class="currency">MXN</span></p>');
                                }elseif ($value->roomPrice->currency == 'EUR') {
                                    echo ('<p style="font-size:" class="col-12 new-price"> $'.number_format(($value->roomPrice->price->net*$tmp->eur), 2, '.', '').'<span class="currency">MXN</span></p>');
                                }
                            }
                            ?>
                        </div>

                        <div class="col-12 border">
                        <p> Precio total:
                        <?php
                                if ($option->price->currency == 'USD') {
                                    echo ('<p style="font-size:" class="col-12 new-price"> $'.number_format(($option->price->net*$tmp->usd), 2, '.', '').'<span class="currency">MXN</span></p>');
                                }elseif ($value->roomPrice->currency == 'EUR') {
                                    echo ('<p style="font-size:" class="col-12 new-price"> $'.number_format(($option->price->net*$tmp->eur), 2, '.', '').'<span class="currency">MXN</span></p>');
                                }
                        ?>
                        </p>
                            <button class="btn btn-outline-success col-12" type="submit"><i class="fas fa-plus-square"></i> Reservar</button>
                        </div>
                    </div>
                    <input type="hidden" name="ref" value="<?php echo $option->id?>">
                    <input type="hidden" name="usd" value="<?php echo $tmp->usd?>">
                    <input type="hidden" name="eur" value="<?php echo $tmp->eur?>">
                    <input type="hidden" name="rooms" value='<?php echo json_encode(${"tRooms".$flagH})?>'>
                    <input type="hidden" name="payType" value="<?php echo $option->paymentType?>">
                </form>
                <!-- ============ -->
                <!-- fin articulo -->
                <!-- ============ -->
                <?php
                $cont++;
                $flagH++;
                 }
                
                ?>
    </div> <!-- row -->
</section>

<section class="container">
    <!-- ancla -->
    <a id="services"></a>
    <div class="row d-flex flex-wrap">
        <section class=" col-sm-12 col-md-12 col-lg-6">
            <article class="col-sm-12 col-md-12 col-lg-12 border">
                <h3 class="colorText2">Información del establecimiento</h3>
                <h4 class="ml-0 mr-0 mt-2 mb-2 colorText1"><?php echo ($mediaHotel->hotels->edges[0]->node->hotelData->hotelName) ?></h4>
                <p class="colorTextP">
                <?php
                    foreach ($mediaHotel->hotels->edges[0]->node->hotelData->descriptions as $description) {
                        if ($description->type == 'GENERAL') {
                            $desText;
                            for ($zz=0; $zz < count($description->texts) ; $zz++) { 
                                if ($description->texts[$zz]->language=='es') {
                                    $desText = $description->texts[$zz]->text;
                                }
                            }
                            echo($desText);
                        }
                    ?>
                    <?php
                    }
                    ?>
                </p>
                <!-- ============== -->
                <!-- mapa del lugar -->
                <!-- ============== -->
                <div id="map" class="map"></div>
                <a href="#" class="amenities"><i class="fas fa-map-marker-alt amenities"></i> <?php echo $mediaHotel->hotels->edges[0]->node->hotelData->location->address.' '.$mediaHotel->hotels->edges[0]->node->hotelData->location->city ?></a>
            </article>
            <article class="col-sm-12 col-md-12 col-lg-12 border">
                <!-- ancla -->
                <a id="politicies"></a>
                <h4 class="colorText1">Políticas</h4>
                <p>
                <?php
                    foreach ($mediaHotel->hotels->edges[0]->node->hotelData->descriptions as $description) {
                        if ($description->type == 'AMENITY') {
                            $desText;
                            for ($zz=0; $zz < count($description->texts) ; $zz++) { 
                                if ($description->texts[$zz]->language=='es') {
                                    $desText = $description->texts[$zz]->text;
                                }
                            }
                            echo($desText);
                        }
                    ?>
                    <?php
                    }
                    ?>
                </p>
            </article>
        </section>
        <section class=" col-sm-12 col-md-12 col-lg-6">
            <article class="col-sm-12 col-md-12 col-lg-12 border">
                <h3>Servicios del establecimiento</h3>
                <?php 
                    foreach ($amenities as $services) {
                        $textServ2;
                     for ($z=0; $z < count($services->texts) ; $z++) { 
                         if ($services->texts[$z]->language == 'es') {
                             $textServ2 = $services->texts[$z]->text;
                         }
                     }
                        ?>
                        <p><i class="fas fa-th-list amenities"></i> <?php echo $textServ2?></p>
                     <?php
                         }
                     ?>
            </article>
        </section>
    </div>
</section>

<!-- ======================================================== -->
<!-- script que genera el mapa del lugar segun las coodenadas -->
<!-- ======================================================== -->

<script>
    var map = L.map('map', {
                fadeAnimation: false,
                zoomAnimation: false,
                markerZoomAnimation: false
            }).setView([<?php echo $mediaHotel->hotels->edges[0]->node->hotelData->location->coordinates->latitude?>, <?php echo $mediaHotel->hotels->edges[0]->node->hotelData->location->coordinates->longitude ?>], 15);

            L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
                maxZoom: 28}).addTo(map);
            L.control.scale().addTo(map);
            L.marker([<?php echo $mediaHotel->hotels->edges[0]->node->hotelData->location->coordinates->latitude?>, <?php echo $mediaHotel->hotels->edges[0]->node->hotelData->location->coordinates->longitude ?> ], {draggable: false}).addTo(map);
            $(function () {
  $('[data-toggle="popover"]').popover()
})
</script>
<?php
    include 'footer.php';
?>