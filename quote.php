<?php
require_once ('model/database.php');//fluentPDO

$controller = 'travelgate';

require_once "controller/$controller.controller.php";
$controller = ucwords($controller).'controller';
$controller = new $controller;

$datas = array('token' => $_POST['ref'],
               'context' => $_POST['context'],
               'access' => $_POST['access'],
               'hName' => $_POST['hName'],
               'usd' => $_POST['usd'],
               'eur' => $_POST['eur'],
               'rooms' => $_POST['rooms'],
               'payType' =>$_POST['payType']
               );
$controller->quotes($datas);
?>