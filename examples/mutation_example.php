<?php

require_once __DIR__ . '/../vendor/autoload.php';

// use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Mutation;
use GraphQL\RawObject;
include('./config.php');
$tokenBook='71@11[190911[190930[1[14[0[US[US[en[USD[0[1[881[1[14[1[1[997.5#0#false#EUR##0#[1|23|1|2019-09-11|19|303410|303411|14|0|0[1293[23[[After@997.5@Before@950@ExpireDate@30/09/2019@mercado@US@tgx_sess@268c1168-e677-4416-bd4a-e9fbbb98a85f';
// Create the GraphQL mutation
$gql = (new Mutation('book'))
        ->setSelectionSet([
            (new Mutation('hotelX'))
            ->setArguments(['book' => new RawObject('{input: 
            {
              optionRefId:"'.$tokenBook.'"
              clientReference: "123456789"
                    deltaPrice: { 
                amount: 209.475 
                percent: 10 
                applyBoth: true 
              },
              paymentCard: {
                cardType: "VI" 
                holder: { 
                  name: "Test" 
                  surname: "Test" 
                }, number: "0123456789101112" 
                CVC: "123" 
                expire: { 
                  month: 8 
                  year: 21 
                } 
              },
              holder: { 
                name: "Test"
                surname: "Test" 
              },
              rooms: [ 
                { 
                  occupancyRefId: 1, 
                  paxes: [
                    {
                      name: "Test1", 
                      surname: "Test1", 
                      age: 23
                    }
                  ]
                }
              ]
            }
            }')])
            ->setSelectionSet(
                [
                   'booking'=>[
                       'status',
                       'reference'=>[
                           'client',
                           'hotel',
                           'supplier',

                       ],
                       'holder'=>[
                           'name',
                           'surname'
                       ],
                       'hotel'=>[
                           'hotelName',
                           'hotel'
                       ]
                   ]
                ]
            )
        ]);

// Run query to get results
try {
    $results = $client->runQuery($gql);
}
catch (QueryError $exception) {

    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}

// Display original response from endpoint
var_dump($results->getResponseObject());

// Display part of the returned results of the object
var_dump($results->getData()->pokemon);

// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
print_r($results->getData()['pokemon']);