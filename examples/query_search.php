<?php
require_once __DIR__ . '/../vendor/autoload.php';
include('./config.php');

// use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;

$checkIn1=$_POST['checkIn'];
$checkOut1=$_POST['checkOut'];
$rooms1=$_POST['rooms'];
// $aduls=$_POST['adults'];
// $childs=$_POST['childs'];

$array = array();
for ($i=1; $i<=$rooms1 ; $i++) { 
  array_push($array,"{paxes:[{age:23},{age:11}]},");
}

$paxes = (implode($array));
// var_dump($paxes);
//query body
$lorem = 'query xmlTravelgate{
  hotelX {
    search(
      criteria:{
        checkIn:"'.$checkIn1.'"
        checkOut:"'.$checkOut1.'"
      	hotels:["2"]
        
        occupancies:
          [
           '.$paxes.'
          ]
        nationality:"US"
        currency:"USD"
        destinations: ["1149", "US", "31256"]
        market:"US"
      },
      settings:{
        businessRules: null
        timeout:24700
        client: "'.$clientMode.'"
        context:"'.$context.'"
        auditTransactions:true
        testMode:true
      },
      filter:{
        access:{
          includes:[
           "881"
            "0"
            
          ],
          excludes:null
        }
      
      }
    ) {
      #auditData {
        #transactions {
          #request
         # response
        #}
      #}
      context
      options {
        surcharges {
          chargeType
          mandatory
          description
          price {
            currency
            binding
            net
            gross
            exchange {
              currency
              rate
            }
            markups {
              channel
              currency
              binding
              net
              gross
              exchange {
                currency
                rate
              }
            }
          }
        }
        accessCode
        supplierCode
        market
        hotelCode
        hotelName
        boardCode
        paymentType
        status
        occupancies {
          id
          paxes {
            age
          }
        }
        
        rooms {
          occupancyRefId
          code
          description
          refundable
          units
          roomPrice {
            price {
              currency
              binding
              net
              gross
              exchange {
                currency
                rate
              }
              markups {
                channel
                currency
                binding
                net
                gross
                exchange {
                  currency
                  rate
                }
              }
            }
          }
          beds {
            type
            description
            count
            shared
          }
          ratePlans {
            code
            name
            effectiveDate
            expireDate
          }
          promotions {
            code
            name
            effectiveDate
            expireDate
          }
        }
        price {
          currency
          binding
          net
          gross
          exchange {
            currency
            rate
          }
          markups {
            channel
            currency
            binding
            net
            gross
            exchange {
              currency
              rate
            }
          }
        }
        
        supplements {
          code
          name
          description
          supplementType
          chargeType
          mandatory
          durationType
          quantity
          unit
          effectiveDate
          expireDate
          resort {
            code
            name
            description
          }
          price {
            currency
            binding
            net
            gross
            exchange {
              currency
              rate
            }
            markups {
              channel
              currency
              binding
              net
              gross
              exchange {
                currency
                rate
              }
            }
          }
        }
        surcharges {
          chargeType
          description
          price {
            currency
            binding
            net
            gross
            exchange {
              currency
              rate
            }
            markups {
              channel
              currency
              binding
              net
              gross
              exchange {
                currency
                rate
              }
            }
          }
        }
        rateRules
        cancelPolicy {
          refundable
          cancelPenalties {
            hoursBefore
            penaltyType
            currency
            value
          }
        }
        remarks
        token
        id
      }
      errors {
        code
        type
        description
      }
      warnings {
        code
        type
        description
      }
    }
  }
}';

// Create the GraphQL query
$gql = <<<QUERY
$lorem
QUERY;

try {
    $results = $client->runRawQuery($gql);
}

catch (QueryError $exception) {
    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}

// Display original response from endpoint
// var_dump($results->getResponseObject());

// Display part of the returned results of the object
// var_dump($results->getData()->hotelX);
$xd = $results->getData()->hotelX;
 
// for ($i=0; $i < count($xd->search->options) ; $i++) { 
  // print_r($i);
  // print_r($xd->search->options[$i]);  
  // echo ('<br>');
  // echo ('<br>');
  // print_r($xd->search->options[$i]->id);  
  // echo ('<br>');
  // print_r($xd->search->options[$i]->hotelName);
  // echo ('<br>');
  // print_r($xd->search->options[$i]->price->net);
  // echo ('<br>');
// }
// print_r($xd->search->options[1]->id);
// var_dump($results->getData()->hotelX);
// var_dump($results->getData()->hotelX->options);

// $rest= json_encode($results->getData()->hotelX);
// $lory = json_decode($rest, true);
// var_dump($lory);
// print_r($lory);

// var_dump($end);
// print_r($rest);

// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
// print_r($results->getData()['hotelX']);
// print_r($results->getData()['hotelX']);
$title = 'Search';
include('../src/template/header.php');
?>
  <section class="container">
    <div class="row">
      <div class="col d-flex flex-row flex-wrap justify-content-between align-items-center">
    
      <?php
       if($xd->search->options!==null){
          for ($i=0; $i < count($xd->search->options) ; $i++) { 
      ?>
        <form class="card col-sm-12 col-md-6 col-lg-6" action="./query_quote.php" method="post">
          <img src="http://lorempixel.com/400/200/nature/<?php $d=mt_rand(1,10);echo $d ;?>" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">
            <?php 
            print_r($i); print_r(' '); print_r(' '); print_r($xd->search->options[$i]->status);            
            ?><br>
            <input type="text" name="hotelName" id="hotelName" value=" <?php print_r($xd->search->options[$i]->hotelName); ?>" style="border:0px;" readonly>
            </h5>     
            <p class="card-text">
              Precio: <span class="card-text text-black-50">
              <?php
                  print_r($xd->search->options[$i]->price->net); echo(' '); print_r($xd->search->options[$i]->price->currency);
              ?>
              </span>
              Bruto: <span class="card-text text-black-50">
              <?php
                   print_r($xd->search->options[$i]->price->gross); echo(' '); print_r($xd->search->options[$i]->price->currency);
              ?>
              </span>
            <?php
                // print_r($xd->search->options[$i]->occupancies)
            ?>
            </p>
            <p class="card-title text-capitalize">
            <span class="text-secondary text-success">Detalles:</span> <br>
            Habitaciones disponibles: <?php echo(count($xd->search->options[$i]->occupancies)) ?> </p>
            <?php
                  for ($i2=1; $i2 <= count($xd->search->options[$i]->occupancies) ; $i2++) { 
                    ?>
                    <p class="card-title text-capitalize text-info">
                       Descripción de la Habitación <?php echo($i2) ?>
                    </p>
                    <p class="card-text text-black-50">
                      <span class="card-text text-dark">Código:</span> <?php echo($xd->search->options[$i]->rooms[$i2-1]->code) ?>
                    </p>
                    <p class="card-text text-black-50">
                      <span class="card-text text-dark">Descripción:</span> <?php echo($xd->search->options[$i]->rooms[$i2-1]->description) ?>
                    </p>
                    <p class="card-text text-black-50">
                      <span class="card-text text-dark">Precio:</span> <?php echo($xd->search->options[$i]->rooms[$i2-1]->roomPrice->price->net) ?>,
                      <span class="card-text text-dark">Bruto:</span> <?php echo($xd->search->options[$i]->rooms[$i2-1]->roomPrice->price->gross) ?>
                    </p>
                    <!-- seccion de beds -->
                      <?php
                      for ($i3=1; $i3 <= count($xd->search->options[$i]->rooms[$i2-1]->beds) ; $i3++) {
                      ?>
                        <p class="card-text text-black-50">
                          <span class="card-text text-dark">Tipo:</span> <?php 
                          print_r($xd->search->options[$i]->rooms[$i2-1]->beds[$i3-1]->type) 
                          ?>,
                          <span class="card-text text-dark">Compartido:</span> <?php 
                          if ($xd->search->options[$i]->rooms[$i2-1]->beds[0]->shared == false) {
                            echo("false");
                            }else {
                              echo("true");
                          }
                      ?>
                        </p>
                    <?php
                    }
                    ?>
                    <!-- seccion de rateplans -->
                    <?php
                      for ($i4=1; $i4 <= count($xd->search->options[$i]->rooms[$i2-1]->ratePlans) ; $i4++) {
                      ?>
                        <p class="card-text text-black-50">
                          <span class="card-text text-dark">Código:</span> <?php 
                          print_r($xd->search->options[$i]->rooms[$i2-1]->ratePlans[$i4-1]->code)?>,
                          <span class="card-text text-dark">Fecha efectiva:</span> <?php 
                          print_r($xd->search->options[$i]->rooms[$i2-1]->ratePlans[$i4-1]->effectiveDate)?>,
                          <span class="card-text text-dark">fecha de expiración:</span> <?php 
                          print_r($xd->search->options[$i]->rooms[$i2-1]->ratePlans[$i4-1]->expireDate)?>
                        </p>
                    <?php
                    }
                    ?>
                     <!-- seccion de promos -->
                    <p class="card-text text-dark">
                    Promociones: <br>
                     <?php
                      for ($i4=1; $i4 <= count($xd->search->options[$i]->rooms[$i2-1]->ratePlans) ; $i4++) {
                        if ($xd->search->options[$i]->rooms[$i2-1]->promotions !== null) {
                      ?>
                      <p class="card-text text-black-50">
                          <span class="card-text text-dark">Código:</span> <?php 
                          print_r($xd->search->options[$i]->rooms[$i2-1]->promotions[$i4-1]->code)?>,
                          <span class="card-text text-dark">Nombre:</span> <?php 
                          print_r($xd->search->options[$i]->rooms[$i2-1]->promotions[$i4-1]->name)?>,
                          <span class="card-text text-dark">fecha efectiva:</span>
                          <?php 
                            if ($xd->search->options[$i]->rooms[$i2-1]->promotions[$i4-1]->effectiveDate !==null) {
                              print_r($xd->search->options[$i]->rooms[$i2-1]->promotions[$i4-1]->effectiveDate);
                            }else {
                              print_r('No aplica');
                            }
                          ?>
                          <span class="card-text text-dark">fecha de expiración:</span> 
                          <?php 
                            if ($xd->search->options[$i]->rooms[$i2-1]->promotions[$i4-1]->expireDate!==null) {
                              print_r($xd->search->options[$i]->rooms[$i2-1]->promotions[$i4-1]->expireDate);
                            }else{
                              print_r('No aplica');
                            }
                          ?>
                      </p>
                    <?php
                    }else{
                      print_r('no aplica');
                    }//fin de for promos
                    }//fin del for ratesPlans
                    ?>
                    </p>
                  <?php
                }
                // si jala, esta parte debo de enviar al quote
                // print_r('Token: ');print_r($xd->search->options[$i]->token);print_r('<br>');
                // print_r('Id: ');print_r($xd->search->options[$i]->id);
            ?>
            <input type="hidden" name="optionRefId" value="<?php echo($xd->search->options[$i]->id)?>">
            <!-- </p> -->
            
            <input class="btn btn-outline-success" type="submit" value="Seleccionar">
          </div>
        </form>
<?php
    }
}else{
  echo('no hay coincidencias con su busqueda');
}
?>
      </div> 
    </div>
  </section>
  <?php
    include('../src/template/footer.php');
  ?>