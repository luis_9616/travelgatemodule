<?php

require_once __DIR__ . '/../vendor/autoload.php';
include('./var.php');

use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;


$checkIn="2019-09-21";
$checkOut="2019-09-30";
$language="en";
$nationality="US";
$currency="USD";
$market="US";
$hotels="1";
$client="Demo_Client";
$context="HOTELTEST";
$age="23";//se debe generar dinamicamente
// Create Client object to contact the GraphQL endpoint
$client = new Client(
    'https://api.travelgatex.com/',
    ["authorization"=>"ApiKey ".$apikey]  // Replace with array of extra headers to be sent with request for auth or other purposes
);


// Create the GraphQL query
$gql = (new Query('query'))
    ->setSelectionSet([//query principal
            (new Query('hotelX'))
            ->setSelectionSet([
                (new Query('search'))
                ->setArguments([
                    (new Query('criteria'))
                    ->setSelectionSet([
                        'checkIn'=>'2019-09-12',
                        'checkOut'=>'2019-09-21',
                        'language'=>'en',
                        'nationality'=>'US',
                        'currency'=>'USD',
                        'market'=>'US',
                        'hotels'=>'1',
                        'occupancies'=>['age'=>'23']
                    ]),
                    (new Query('settings'))
                    ->setSelectionSet([
                        'timeout'=>'24700',
                        'client'=>'perseusdirect',
                        'context'=>'HOTELTEST',
                        'testMode'=>true
                    ]),
                    (new Query('filter'))
                    ->setSelectionSet([
                        'rateRules'=>[
                            'includes'=>[
                                'NORMAL'
                            ]
                        ]
                    ])
                ])
            ])
]);//query principal

// Run query to get results
try {
    $results = $client->runQuery($gql);
}
catch (QueryError $exception) {

    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}

// Display original response from endpoint
var_dump($results->getResponseObject());

// Display part of the returned results of the object
var_dump($results->getData()->hotelX);

// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
print_r($results->getData()['hotelX']);