<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Travelgate: <?php echo($title)?></title>
    <!-- bootstrap -->
    <!-- <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- restricciones -->
    <script type="text/javascript" src="../openpaymodulo/assets/js/onlyNumbers.js"></script>
    <!-- openpay -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
    <script type='text/javascript' src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>
    <script type="text/javascript" src="../openpaymodulo/assets/js/script.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
    <!-- <script type='text/javascript' src="../../assets/js/validateCards.js"></script> -->
    <script>
        $(document).ready(function () {
            // titular
            $("#name_on_card").keyup(function () {
                var value = $(this).val();
                $("#titular2").val(value);
            });
            // titular
            $("#card").keyup(function () {
                var value = $(this).val();
                $("#tarjeta2").val(value);
            });
        });
    </script>
    <style>
    .required{
        border:4px red solid !important;
    }
    </style>
</head>