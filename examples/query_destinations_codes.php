<?php
require_once __DIR__ . '/../vendor/autoload.php';
include('./config.php');

// use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;


$tokenDestinations = '';
 $queryText = 'query fliterSearch{
    hotelX {
      destinations(
        token:"'.$tokenDestinations.'"
    criteria: {
      maxSize: '.$sizeDestinations.'
      access: "'.$accessSupplier.'",
     
   
    },
    relay:{}
  ) {
        token
        edges {
          cursor
          node {
            code
            destinationData {
              code
              available
              destinationLeaf
              closestDestinations
              parent
              type
              texts {
                language
                text
              }
            }
            
            error{
              code
              type
              description
            }
            createdAt
            updatedAt
          }
        }
      }
    }
  }';
// Create the GraphQL query
$gql = <<<QUERY
$queryText
QUERY;

try {
    $results = $client->runRawQuery($gql);
    $xd = $results->getData()->hotelX;
    var_dump($xd);
}

catch (QueryError $exception) {
    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}

