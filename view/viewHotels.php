<?php
    include('header.php');
    echo count($searches->search->options);
    ?>    
    <!-- ================================ -->
    <!-- caja para realizar otra busqueda -->
    <!-- ================================ -->
    
    <section class="wrap2 card">
		<div class="boxSearch99 card-body p-0 pt-3">     
                <form class="formMy"  action="http://travelgatemodule.u/searches.php" method="POST">
                
                    <input id="arrayLang" name="langPage" type="hidden" value="<?php echo $datas['lang'] ?>">
                    <div id="lorem" class="form-group col-sm-12 col-md-12 col-lg-5">
                        <label class="labelMin" for="busqueda">Destino</label>
                        <i class="innerIcon fas fa-search-location"></i>
                        <input id="destinyName"
                        class="bxSearch form-control form-control-lg" 
                        type="text" 
                        class="form-control"
                        placeholder="Buscar..." 
                        autocomplete="off" 
                        value="<?php echo $datas['destino'] ?>"
                        name="busqueda" >       
                        <!-- SELECT PARA TRAER LOS DESTINOS QUE TENGO EN LA BASE DE DATOS -->
                        <div class="tabla_resultado"></div>
                        <!-- SELECT PARA TRAER LOS DESTINOS QUE TENGO EN LA BASE DE DATOS -->
                    </div>
                    <div class="aaa col-sm-6 col-md-6 col-lg-2">
                        <label class="labelMin labelMin2" for="checkIn">Check In</label>
                        <i class="innerIcon fas fa-calendar-day"></i>
                        <input type="date" 
                            class="form-control checks" 
                            name="checkIn" 
                            id="checkIn"
                            value="<?php echo $datas['checkIn'] ?>"
                            required>
                    </div>
                    <div class="aaa col-sm-6 col-md-6 col-lg-2">
                        <label class="labelMin labelMin2" for="checkOut">Check Out</label>
                        <i class="innerIcon fas fa-calendar-day"></i>
                        <input type="date" 
                            class="form-control checks" 
                            name="checkOut" 
                            id="checkOut" 
                            value="<?php echo $datas['checkOut'] ?>"
                            required>
                    </div>
                    <div class="aaa col-sm-6 col-md-6 col-lg-2">
                        <label class="labelMin" for="rooms">Habitaciones</label>
                        <i class="innerIcon fas fa-bed"></i>
                        <Select name="rooms" class=" selPad checks form-control">
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </Select>
                    </div>
                    <!-- <div class="w-100"></div> -->
                    <div class="form-group col-sm-6 col-md-6 col-lg-1">
                        <input id="btnGO" class="btn btn-success  btn-lg p-0 pt-2 pb-2 pl-1 pr-1" type="submit" value="Buscar">
                    </div>
                </form>
		</div>
	</section>
    
      <!-- ======================================================= -->
      <!-- contenedor donde sucede toda la magia de los resultados -->
      <!-- ======================================================= -->
    <section class="wrap">
		<div class="store-wrapper">
            
            <!-- =============================================== -->
            <!-- contenedor del menu para filtrar los resultados -->
            <!-- =============================================== -->
			<aside id="menuToolBox" class="category_list">
				<a href="#" class="toolOption category_item" category="all"><i class="fas fa-globe-americas"></i> Mostrar todo</a><!-- <a href="#" class="category_item btn" category="all">Todo</a> -->
                <!-- <a href="#" class="category_item btn" category="ordenadores">Menor a mayor</a> --> <!-- <a href="#" class="category_item btn" category="laptops">Mayor a menor</a> --> <!-- <a href="#" class="category_item btn" category="smartphones">Categoria</a> -->

                <!-- ========================= -->
                <!-- filtro por palabras clave -->
                <!-- ========================= -->
                <div class="toolOption">
                    <label for="hotelsSearchBox"><i class="fas fa-hotel"></i> Buscar por hoteles</label>
                    <input id="filtrar" 
                           type="text" 
                           name="hotelsSearchBox" 
                           class="form-control" 
                           placeholder="Nombre del hotel">
                </div>

                <!-- ================== -->
                <!-- filtro por precios -->
                <!-- ================== -->
               <div class="toolOption">
                    <div class="custom-control custom-radio">
                      <input type="radio" 
                             id="customRadio2" 
                             name="customRadio" 
                             class="custom-control-input" value="optionMinor">
                      <label class="custom-control-label" for="customRadio2">Mayor costo primero <i class="fas fa-arrow-up"></i></label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input type="radio" 
                             id="customRadio1" 
                             name="customRadio" 
                             class="custom-control-input" 
                             value="optionMajor">
                      <label class="custom-control-label" for="customRadio1">Menor costo primero <i class="fas fa-arrow-down"></i></label>
                    </div>
               </div>
               
                <!-- ==================== -->
                <!-- filtro por estrellas -->
                <!-- ==================== -->
                <div class="boxStars">
                    <ul id="filterStars" class="ks-cboxtags">
                        <li><input name="star" type="checkbox" id="star1" value="s1"><label for="star1">1</label></li>
                        <li><input name="star" type="checkbox" id="star2" value="s2"><label for="star2">2</label></li>
                        <li><input name="star" type="checkbox" id="star3" value="s3"><label for="star3">3</label></li>
                        <li><input name="star" type="checkbox" id="star4" value="s4"><label for="star4">4</label></li>
                        <li><input name="star" type="checkbox" id="star5" value="s5"><label for="star5">5</label></li>
                    </ul>
                </div>
            </aside>
            
            <!-- ============================ --> <!-- ============================ --> <!-- ============================ -->
            <!-- caja padre de los resultados --> <!-- caja padre de los resultados --> <!-- caja padre de los resultados -->
            <!-- ============================ --> <!-- ============================ --> <!-- ============================ -->
			<section id="divBox" class="products-list">
                
                <!-- ======== -->
                <!-- articulo -->
                <!-- ======== -->
    <?php
            $hcodes = array();
            $haccess = array();
            if ($searches->search->options != null) {
            foreach ($searches->search->options as $option) {
                $mediaHotel = $this->model->getDataHotels($option->accessCode, $option->hotelCode);
                // $occupancy = $option->occupancies;
                // $rooms = $option->rooms;
                	// echo('<pre>');
                    // var_dump($occupancy);
                    // var_dump($rooms);
                    // echo('</pre>');
                    // conteo de los arrays
                    // echo count($occupancy);
                    // echo count($rooms);
                $stars = $mediaHotel->hotels->edges[0]->node->hotelData->categoryCode;
    ?>
    <?php
                // $mediaHotel = $this->model->getDataHotels($option->accessCode, $option->hotelCode);
        if (!in_array($option->hotelCode, $hcodes)) {
            foreach ($option->rooms as $prmos) {
                if ($prmos->promotions !== null) {
                    $dataPromo ='Si';
                }else{
                    $dataPromo ='No';
                }
            }
            ?>
			    <form action="http://travelgatemodule.u/hotel.php" 
                      method="post" 
                      class="product-item col-sm-12 col-md-12 col-lg-12" 
                      data-price="<?php  if ($option->price->currency == 'USD') {
                                echo number_format(($option->price->net*$tmp->usd), 2, '.', '');
                                // echo $option->price->currency;
                            }elseif ($option->price->currency == 'EUR') {
                                echo number_format(($option->price->net*$tmp->eur), 2, '.', '');
                                // echo $option->price->currency;
                            } ?>" 
                      data-cateCode="<?php echo($stars) ?>" 
                      data-hotelName="<?php echo($option->hotelName)?>" 
                      data-promo="<?php echo $dataPromo?>"
                      category=""
                      target="">
                    <!-- ========= -->
                    <!-- seccion 1 -->
                    <!-- ========= -->
                    
                    <div class=" col-sm-12 col-md-12 col-lg-4 p-0">
                        <div id="carouselExampleControls<?php echo $option->hotelCode?>" class="carousel slide " data-ride="carousel">
                            <div class="carousel-inner">
                                <?php                                
                                for($i=0; $i < count($mediaHotel->hotels->edges[0]->node->hotelData->medias); $i++) {
                                    $imgz = $mediaHotel->hotels->edges[0]->node->hotelData->medias;
                                    if ($i == 0) {
                                ?>
                                    <div class="carousel-item active">
                                        <img src="<?php echo ($imgz[$i]->url); ?>" class="d-block" alt="..." width="1100" height="500">
                                    </div>
                                    <?php
                                    }else {
                                    ?>
                                        <div class="carousel-item">
                                            <img src="<?php echo ($imgz[$i]->url); ?>" class="d-block" alt="..." width="1100" height="500">
                                        </div>
                                <?php
                                    }
                                    if ($i == 1) {
                                    break;
                                    }
                                }
                                ?>
                                <!-- <div class="carousel-item active">
                                    <img src="../src/img/hotels-1.jpg" class="d-block" alt="...">
                                </div> -->
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls<?php echo $option->hotelCode?>" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls<?php echo $option->hotelCode?>" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                    <!-- ========= -->
                    <!-- seccion 2 -->
                    <!-- ========= -->
                    <div class="detail boxCol col-sm-12 col-md-12 col-lg-6">
                        <h4 class="htl-name" ><?php echo $option->hotelName?></h4>
                        <?php
                        if ($stars == 'S1') {
                            echo str_repeat('<i class="fas fa-star"></i>',1);
                           }elseif ($stars == 'S15') {
                            echo str_repeat('<i class="fas fa-star"></i>',1) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S2') {
                            echo str_repeat('<i class="fas fa-star"></i>',2);
                           }elseif ($stars == 'S25') {
                            echo str_repeat('<i class="fas fa-star"></i>',2) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S3') {
                            echo str_repeat('<i class="fas fa-star"></i>',3);
                           }elseif ($stars == 'S35') {
                            echo str_repeat('<i class="fas fa-star"></i>',3) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S4') {
                            echo str_repeat('<i class="fas fa-star"></i>',4);
                           }elseif ($stars == 'S45') {
                            echo str_repeat('<i class="fas fa-star"></i>',4) . '<i class="fas fa-star-half"></i>';
                           }elseif ($stars == 'S5') {
                            echo str_repeat('<i class="fas fa-star"></i>',5);
                           }else {
                               echo $stars;
                           }
                        ?>
                        <p class="border" style="font-size:12px; width:100%; overflow:hidden">
                        <?php
                            echo $option->remarks;
                        ?>
                        </p>
                        <!-- <span class="htl-type">Amenities</span>
                        <ul>
                            <li></li>
                            <li></li>
                        </ul> -->
                    </div>

                    <!-- ========= -->
                    <!-- seccion 3 -->
                    <!-- ========= -->
                    <div class="cash boxCol col-sm-12 col-md-12 col-lg-2">
                        <div class="promo">
                            <!-- <span class="text-promo">% - pronta reserva</span> -->
                            <?php
                            $flag1 =0;
                            foreach ($option->rooms as $prmos) {
                                if ($prmos->promotions !== null) {
                                    echo '<span class="text-promo">Con Promoción</span>';
                                    if ($flag1=1) {
                                    break;
                                    }
                                }else{
                                    echo '<p class="bg-info"></p>';
                                }
                                $flag1++;
                            }
                        ?>
                        </div>
                        <div class="real-price">
                            <!-- <div class="old-price">$400 .<span class="currency">MXN</span></div> -->
                            <div class="new-price">$ <?php 
                            	// echo('<pre>');
                                //variable a inmprimir
                                    // var_dump($option->price->currency);
                                // echo('</pre>');
                            if ($option->price->currency == 'USD') {
                                echo number_format(($option->price->net*$tmp->usd), 2, '.', '');
                                // echo $option->price->currency;
                            }elseif ($option->price->currency == 'EUR') {
                                echo number_format(($option->price->net*$tmp->eur), 2, '.', '');
                                // echo $option->price->currency;
                            }
                            ?> <span class="currency">MXN</span></div>
                        </div>
                        <input type="hidden" value='<?php echo json_encode($datas) ?>' name="datas">
                        <input type="hidden" value="<?php echo ($option->hotelCode) ?>" name="HotelCode">
                        <div class="btn-book">
                            <button class="mt-5 btn btn-outline-success" type="submit"><i class="fas fa-plus-square"></i> Ver más</button>
                        </div>
                    </div>
                </form>
                <!-- ============ -->
                <!-- fin articulo -->
                <!-- ============ -->
                <?php
                    }
                array_push($haccess, $option->accessCode);
                array_push($hcodes, $option->hotelCode);
               }//  fin foreach
            }// fin if valida foreach
            else {
                // echo "<script>alert('Esto no jala, FUGA!!!')</script>";
                echo "<script>location.reload();</script>";
                // $archivoActual = $_SERVER['PHP_SELF'];
                // header('refresh:1;url='.$archivoActual);
            }

            // ===========================================================================================
            // funcion para capturar los codigos de hoteles e insertarlos a la base de datos para proceder
            // con la obtencion de los datos estaticos de cada hotel
            // ===========================================================================================

                // for ($i=0; $i <count($hcodes) ; $i++) { 
                //     echo 'Comprobar si existe en la base de datos el codigo: '.$hcodes[$i];
                //     	echo('<br>');
                //         echo('<br>');
                //     ${"testExist".$i} = $this->model2->verifyHCodes($hcodes[$i], $haccess[$i]);
                //     ${"testExist".$i} = $this->model2->verifyHo('hotels', $hcodes[$i], $haccess[$i]);
                //     if (empty(${"testExist".$i})) {
                //         ${"insertCode".$i} = $this->model2->insertDataGrnl('tmp_hcodes',$hcodes[$i], $haccess[$i]);
                //         echo('<br> codigo de hotel '.$hcodes[$i].' se ha insertado <br> total de datos insertados: '.${"insertCode".$i}.'<br>');
                //     }else {
                //         echo '<br> El codigo: '.$hcodes[$i].' ya existe! <br>';
                //     }
                // }
                ?>
            </section>
            
            <!-- caja padre de los resultados --> <!-- caja padre de los resultados --> <!-- caja padre de los resultados -->
		</div>
	</section>
<?php
    // include('footer.php');
?>