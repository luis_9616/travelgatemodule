      for ($i=0; $i < count($edges); $i++){
      ?>
    <article class="card" style="width: 18rem;">
      <div class="card-header">Code: 
        <?php
        echo($edges[$i]->node->destinationData->code);
        ?>
        <p class="card-subtitle">Parent: 
        <?php
        echo($edges[$i]->node->destinationData->parent);
        ?>
        </p>
        <p class="card-subtitle">Type: 
        <?php
        echo($edges[$i]->node->destinationData->type);
        ?>
        <p class="card-subtitle">
        <?php
        if ($edges[$i]->node->destinationData->available == true) {
          // var_dump($edges[$i]->node->destinationData->available);
          echo('Disponible');
        }else {
          echo('No disponible');
        }
        ?>
        </p>
        <p class="card-subtitle">
        <?php
        for ($z=0; $z  < count($edges[$i]->node->destinationData->texts) ; $z++) {
            if ($edges[$i]->node->destinationData->texts[$z]->language == 'es') {
                echo(($edges[$i]->node->destinationData->texts[$z]->text));
                echo('<br>');
            }
            if ($edges[$i]->node->destinationData->texts[$z]->language == 'en') {
                echo(($edges[$i]->node->destinationData->texts[$z]->text));
            }
        }
        // echo($edges[$i]->node->destinationData->text);
        ?>
        </p>
      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item"> DestinationLeaf
            <?php
            if (isset($edges[$i]->node->destinationData->destinationLeaf)) {
             
            for ($i1=0; $i1 <count($edges[$i]->node->destinationData->destinationLeaf); $i1++) { 
             ?>
             <ul>
                <li> 
                <?php
                        echo(($edges[$i]->node->destinationData->destinationLeaf[$i1]));
                ?>
                </li>
             </ul>
             <?php
            }
          }else {
            var_dump($edges[$i]->node->destinationData->destinationLeaf);
          }
            ?>
        </li>
      </ul>
      <ul class="list-group list-group-flush pt-lg-3">
        <li class="list-group-item"> closesDestinations
            <?php
            for ($i2=0; $i2 <count($edges[$i]->node->destinationData->closestDestinations); $i2++) { 
             ?>
             <ul>
                <li> 
                <?php
                        echo(($edges[$i]->node->destinationData->closestDestinations[$i2]));
                ?>
                </li>
             </ul>
             <?php
            }
            ?>
        </li>
      </ul>
    </article>
    <?php
    }
   //backup show destinations