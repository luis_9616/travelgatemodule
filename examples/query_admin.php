<?php
require_once __DIR__ . '/../vendor/autoload.php';
include('./var.php');

use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;


$client = new Client(
    'https://api.travelgatex.com/',
    ["authorization"=>"ApiKey ".$apikey] 
);

$gql=(new Query('query'))
    ->setSelectionSet(
        [
            (new Query('admin'))
                ->setSelectionSet([
                    (new Query('clients'))
                    ->setSelectionSet(
                        [
                            (new Query('edges'))
                                ->setSelectionSet(
                                    [
                                        (new Query('node'))
                                            ->setSelectionSet(
                                                [
                                                    'code',
                                                    (new Query('clientData'))
                                                        ->setSelectionSet(
                                                            [
                                                                'code',
                                                                'name',
                                                                (new Query('group'))
                                                                    ->setSelectionSet(
                                                                        [
                                                                            'code'
                                                                        ]
                                                                    )
                                                            ]
                                                        )
                                                ]
                                            )
                                    ]
                                )                    
                        ]
                ),
                (new Query('accesses'))
                    ->setSelectionSet(
                        [
                            (new Query('edges'))
                                ->setSelectionSet(
                                    [
                                        (new Query('node'))
                                            ->setSelectionSet(
                                                [
                                                    (new Query('accessData'))
                                                        ->setSelectionSet(
                                                            [
                                                                'code',
                                                                (new Query('supplier'))
                                                                    ->setSelectionSet(
                                                                        [
                                                                            'code'
                                                                        ]
                                                                    )
                                                            ]
                                                        )
                                                ]
                                            )
                                    ]
                                )
                        ]
                    )
            ])
        ]
    );

try {
    $results = $client->runQuery($gql);
}
catch (QueryError $exception) {
    print_r($exception->getErrorDetails());
    // var_dump($exception);
    exit;
}

var_dump($results->getResponseObject());

var_dump($results->getData()->admin);

var_dump($results);
$results->reformatResults(true);
print_r($results->getData()['admin']);