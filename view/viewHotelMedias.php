<?php
    // traer los codigos de los hoteles de la tabla temporal
    $row = $medias->fetchAll('id', 'codes, access_sup');
    if (!empty($row)) {
        // recorrer todos los registros de la tabla temporal
        $aux = 0;
        foreach ($row as $hcode) {
            $media = $this->model2->getDataHotels($hcode['access_sup'],$hcode['codes']);
            $datas = $media->hotels->edges[0]->node->hotelData;

            echo($hcode['id']).'<br>';
            echo($hcode['codes'].'<br>');
            // validar si existe en la base de datos
            $testExist = $this->model->verifyDatas("hotels","supplier_access", $hcode['access_sup'], "hotel_code", $datas->hotelCode);

            if (empty($testExist)) {
                // queries para la insercion de los datos
                $datasHotel = array('hotel_code' => $datas->hotelCode, 
                                    'hotel_name' => $datas->hotelName,
                                    'supplier_access' => $hcode['access_sup']);
                // insertar datos de hotel primero
                $insertHotel = $this->model->insertAnyDatas('hotels', $datasHotel);
                echo($insertHotel.'<br>');
                if (!empty($insertHotel)) {
                    for ($z=0; $z < count($datas->descriptions); $z++) { 
                        if ($datas->descriptions[$z]->type == "GENERAL") {
                            for ($i=0; $i < count($datas->descriptions[$z]->texts); $i++) { 
                                if ($datas->descriptions[$z]->texts[$i]->language == 'en') {
                                    $textEn = ($datas->descriptions[$z]->texts[$i]->text);
                                }
        
                                if ($datas->descriptions[$z]->texts[$i]->language == 'es') {
                                    $textEs = ($datas->descriptions[$z]->texts[$i]->text);
                                }
                            }
                        }
                    }
                    if ($datas->location->zipCode != null) {
                        $zipCode = $datas->location->zipCode;
                    }else{
                        $zipCode = '';
                    }
                    $location = array('id_hotel' => $insertHotel, 
                                    'address' => $datas->location->address, 
                                    'city' => $datas->location->city, 
                                    'zip_code' => $zipCode, 
                                    'latitude' => $datas->location->coordinates->latitude, 
                                    'longitude' => $datas->location->coordinates->longitude);

                    $amenities = array('id_hotel' => $insertHotel, 
                                    'all_amenities' => json_encode($datas->amenities));
                    
                    $medias = array('id_hotel' => $insertHotel, 
                                    'all_medias' => json_encode($datas->medias));                                   
                    $details = array('id_hotel' => $insertHotel, 
                                    'category' => $datas->categoryCode, 
                                    'hotel_description_en' => $textEn,'hotel_description_es' => $textEs);
                    // insertar locacion del hotel
                    $insertLocation = $this->model->insertAnyDatas('hotel_location', $location);
                    echo($insertLocation);
                    echo('<br>');
                    // insertar amenities del hotel
                    $insertAmenities = $this->model->insertAnyDatas('hotel_amenities', $amenities);
                    echo($insertAmenities);
                    echo('<br>');
                    // insertar medias del hotel
                    $insertMedias = $this->model->insertAnyDatas('hotel_medias', $medias);
                    echo($insertMedias);
                    echo('<br>');
                    // insertar detalles del hotel
                    $insertDetails = $this->model->insertAnyDatas('hotel_details', $details);
                    echo($insertDetails);
                    echo('<br>');

                    // eliminar de la tabla temporal
                    $deleted = $this->model->deleteAny('tmp_hcodes', $hcode['id'], 'id');
                    var_dump($deleted);
                }
            }else {
                echo 'el hotel ya existe en la base de datos! <br>';
                $deleted = $this->model->deleteAny('tmp_hcodes', $hcode['id'], 'id');
                var_dump($deleted);
            }
            echo '<br>__________________________________________________<br>';
            $aux++; // if ($aux===6) { break; }
        }
    }else{
        echo 'No hay datos para ingresar!';
    }
?>