<?php
require_once 'model/travelgate.php';
require_once 'model/mapping.php';

class travelgateController{
    private $model;
    private $model2;

    public function __CONSTRUCT(){
        $this->model = new Travelgate();
        $this->model2 = new Mapping();
    }
    public function Index(){
        // desing title for the page
        $title = 'Home';
        //create a graphql client for make a queries to travelgate
        // $cred = $this->model->Credential('1');

        $client = $this->model->getClient();
        // print_r($client);
        require_once 'view/header.php';
        require_once 'view/home.php';
        require_once 'view/footer.php';
    }

    // ==========================================    
    // funcion para hacer las busquedas de prueba
    // ==========================================
    public function searches($checkIn, $checkOut, $rooms){
        $title = 'Busqueda';
        $criteria = array($checkIn, $checkOut, $rooms);
        $searches = $this->model->makeSearch($criteria);
        require_once 'view/header.php';
        require_once 'view/viewSearch.php';
        require_once 'view/footer.php';
    }

    // =====================================================================================================================
    // funcion para mostrar la seccion del usuario sobre el hotel eligido para mostrar a más detalle los resultados elegidos
    // =====================================================================================================================
    public function viewResultSelected(){
        require_once 'view/header.php';
        require_once 'view/resultSelected.php';
        require_once 'view/footer.php';
    }
    
    // ================================================================================================================================
    // funcion para llamar a todos los resultados del destino elegido // funcion para llamar a todos los resultados del destino elegido 
    // ================================================================================================================================
    public function searchesAllHotels($datas){
        $title = $datas['destino'];
        // var_dump($datas);
        // require_once 'view/header.php';
        
        // ==========================================
        // asgina el lenguaje para hacer la solicitud
        // ==========================================
        if ($datas['lang'] == '/') {
            $settingCurrency = array('lang' => 'ES', 
                                     'nationality' => 'MX', 
                                     'currency' => 'MXN', 
                                     'market' => 'MX');
        }
        elseif ($datas['lang'] == '/en') {
            $settingCurrency = array('lang' => 'EN', 
                                     'nationality' => 'US', 
                                     'currency' => 'USD', 
                                     'market' => 'US');
        }else {
            $settingCurrency = array('lang' => 'EN', 
                                     'nationality' => 'US', 
                                     'currency' => 'USD', 
                                     'market' => 'US');
        }

        // =======================================
        // obtenemos los datos del destino elegido
        // =======================================
        // $datDestiny = $this->model2->defaultSelect2('detinations_by_suppliers','id_destinations', $datas['datSupplier']);
            // echo('<pre>');
                // var_dump($datDestiny);
            // echo('</pre>');

        // =================================
        // obtenemos los codigos de destinos
        // =================================
        $tmp = json_decode($datas['datSupplier']);
        // $codeDestiny = $this->model2->defaultSelect2('codes_destinations','id_destinations', $tmp->id_destiny);
        $leaf = array();
        $closes = array();
            $uaxCodes = array('leaf' =>json_decode($tmp->leaf),
                           'closes' => json_decode($tmp->closes));
                           
                            foreach ($uaxCodes['leaf'] as $cod) {
                                if (strpos($cod, '#')!== false) {
                                    array_push($leaf, substr($cod, 2));
                                }else {
                                    array_push($leaf, $cod);
                                }
                            }
                            foreach ($uaxCodes['closes'] as $cod1) {
                                if (strpos($cod1, '#')!== false) {
                                    array_push($closes, substr($cod1, 2));
                                }else {
                                    array_push($closes, $cod1);

                                }
                            }
            // echo('<pre>');
            // echo 'arrays filtrados: <br>';
                // var_dump($leaf);
                // var_dump($closes);
            // echo('</pre>');
        $codes = array('leaf' => $leaf, 
                       'closes' => $closes);
        // ================================================================================
        // obtenemos los datos del proveedor y proximamente el costo agregado por proveedor
        // ================================================================================
        // $datSupplier = $this->model2->defaultSelect2('suppliers', 'id_prov', $datDestiny[0]['id_supplier']);
        //     $dataSupplier = array('code' => $datSupplier[0]['supplier_code'], 
        //                           'access' => $datSupplier[0]['access_code']);
            // echo('<pre>');
                // var_dump($dataSupplier);
            // echo('</pre>');
        // ===============================================================================================================================
        // Query que envia los datos necesarios para obtener los datos de destinos optenidos segun la busqueda seleccionada por el usuario
        // ===============================================================================================================================
        $searches = $this->model->getAllHotelByDestiny($datas, $settingCurrency, $codes);
        
        // ==================================================
        // el cambio de monedas esta en $tmp->usd o $tmp->eur
        // ==================================================
        require_once 'view/viewHotels.php';
        require_once 'view/footer.php';
    }

    // ============================================================================================================
    // funcion para obtener solo los resultados de un hotel // funcion para obtener solo los resultados de un hotel
    // ============================================================================================================
    public function searchOnlyHotel($datas, $hotelCode){
        $datas = json_decode($datas);
        $title = $datas->destino;
        // require_once 'view/header.php';
        $tmp = json_decode($datas->datSupplier);
        // ==========================================
        // asgina el lenguaje para hacer la solicitud
        // ==========================================
        if ($datas->lang == '/') {
            $settingCurrency = array('lang' => 'ES', 
                                     'nationality' => 'MX', 
                                     'currency' => 'MXN', 
                                     'market' => 'MX');
        }
        elseif ($datas->lang == '/en') {
            $settingCurrency = array('lang' => 'EN', 
                                     'nationality' => 'US', 
                                     'currency' => 'USD', 
                                     'market' => 'US');
        }else {
            $settingCurrency = array('lang' => 'EN', 
                                     'nationality' => 'US', 
                                     'currency' => 'USD', 
                                     'market' => 'US');
        }

        // ===============================================================================================================================
        // Query que envia los datos necesarios para obtener los datos de destinos optenidos segun la busqueda seleccionada por el usuario
        // ===============================================================================================================================
        $searches = $this->model->getOnlyRoomsByHotel($datas, $settingCurrency, $hotelCode);
        $mediaHotel = $this->model->getDataHotels($tmp->sup_access, $hotelCode);
        
        require_once 'view/resultSelected.php';
        require_once 'view/footer.php';
    }

    // ================================================================================================
    // Función para realizar la comprobacion del hotel // Función para realizar la comprobacion del hotel
    // ================================================================================================
    public function quotes($datas){
        $title='Qoute';
        $quotes = $this->model->makeQuote($datas);
        $dat = json_encode($quotes);
        $datas = json_encode($datas);
        header("location: http://openpaymodulo.u/?quote=".urlencode($dat)."&datas=".urlencode($datas));
    }
}
?>