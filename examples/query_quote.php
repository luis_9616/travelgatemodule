<?php

require_once __DIR__ . '/../vendor/autoload.php';
include('./config.php');

// use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;

if (!empty($_POST['optionRefId'])) {
  $optionRefId = ($_POST['optionRefId']);
  $hotelName = $_POST['hotelName'];
  }else{
    echo('No ha seleccionado una opción o ...');
}
// Create Client object to contact the GraphQL endpoint

$tokenSelected = $optionRefId;
$lorem = '
query quote{
    hotelX{
      quote(
        criteria:{
          optionRefId:"'.$tokenSelected.'"
        },
        settings:{
          client: "'.$clientMode.'"
          testMode:true
          context:"'.$context.'"
        },
        
      ){
        optionQuote{
          optionRefId
          status
          price{
            currency
            net
            gross
          }
          surcharges{
            description
            price{
              currency
              net
              gross
              binding
              markups{
                channel
              }
            }
          }
          cancelPolicy{
            refundable
          }
          remarks
        }
        errors{
          code
          type
          description
        }
        warnings{
          code
          type
          description
        }
      }
    }
  }
';

// Create the GraphQL query
$gql = <<<QUERY
$lorem
QUERY;

try {
    $results = $client->runRawQuery($gql);
}

catch (QueryError $exception) {
    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}

$xd = $results->getData()->hotelX;

$title='Quote';
 include('../src/template/header.openpay.php');
 $importe = $xd->quote->optionQuote->price->gross;
 $currency = $xd->quote->optionQuote->price->currency;
 ?>
<section class="container">
<!-- contenedor principal -->
  <div class="row mt-5 col-lg-9 col-md-12 col-sm-12 col-xs-12 m-auto">
  <!-- logo -->
    <img class="mt-1 mb-2" src="../openpaymodulo/assets/img/GOTOUR_LOGO.png" width="100" alt="Logo Gotour" srcset="">
    <!-- card de pagos -->
      <div class="card-group">
    <!-- card detalles de pago -->
                <aside class="card col-lg-4 col-md-12 col-sm-12 col-xs-12 mb-0 p-0">
                    <div class="card-header">
                        <h5 class="card-title">Detalles</h5>
                    </div>
                    <div class="card-body bg-light">
                        <h5 class="card-title">Cliente:</h5>
                        <p class="card-text"><?php
                        $d=mt_rand(1,1000);
                        $cliente = 'cliPerseus'.$d;
                        echo($cliente);
                        ?></p>
                        <p class="card-text"><?php 
                        // echo($correo);
                        ?></p>
                        <h5 class="card-title">Concepto:</h5>
                        <p class="card-text">
                          Status: 
                          <?php
                              echo($xd->quote->optionQuote->status); echo('.');
                          ?><br>
                          <?php 
                            echo('Hotel: '.$hotelName.'.');
                          ?><br>
                          Refundable: 
                          <?php 
                          if ($xd->quote->optionQuote->cancelPolicy->refundable==false) {
                            echo('false');
                          }else{
                          echo('true');} 
                          ?>

                        </p>
                        <h5 class="card-title">Importe:</h5>
                        <p class="card-text text-monospace text-success" style="font-size:30px">$<?php echo($importe) ?><span style="font-size:15px"> <?php echo($currency) ?> </span></p>
                    </div>
                </aside>
                <!-- card detalles de la tarjeta -->
                <form action="./query_book.php" method="post" id="form" class="card col-lg-8 col-md-12 col-sm-12 col-xs-12 p-0">
                    <input type="hidden" name="token_id" id="token_id">
                    <input type="hidden" name="deviceName" id="deviceName">
                    <input type="hidden" name="cantidad" id="cantidad" value="<?php echo($importe);?>">
                    <input type="hidden" name="descrip" id="descrip" value="<?php echo($hotelName);?>">
                    <input type="hidden" name="perseuscli" id="perseuscli" value="<?php echo($cliente);?>">
                    <input type="hidden" name="correo" id="correo" value="<?php 
                    // echo($correo);
                    ?>">

                    <div class="card-header border-0 bg-white">
                        <h5 class="card-title" >Pago con tarjeta</h5>
                    </div>
                    <div class="card-body"> <!-- body card details -->
                        <section class="p-0 d-flex flex-row flex-wrap">
                            <div class="col-lg-5 col-md-12 col-sm-12 p-0 border border-right-0">
                                <h6 class="text-center">Tarjetas de crédito</h6>
                                <img src="../openpaymodulo/assets/img/cards1.png" alt="cards 01">
                            </div>
                            <div class="col-lg-7 col-md-12 col-sm-12 p-0 border border-left-0">
                                <h6 class="text-center">Tarjetas de debíto</h6>
                                <img src="../openpaymodulo/assets/img/cards2_1.png" alt="cards 02">
                                <img src="../openpaymodulo/assets/img/cards2_2.png" alt="cards 02">
                                <img src="../openpaymodulo/assets/img/cards2_3.png" alt="cards 02">
                            </div>
                        </section>
                        
                        <section class="col-12 d-flex flex-row flex-wrap p-0 mt-4">
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label for="name_on_card">Nombre del titular</label>
                                <input type="text" class="form-control" id="name_on_card" placeholder="Como aparece en la tarjeta" autocomplete="off" data-openpay-card="holder_name" value="" name="fullName">                    
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label for="card">Número de la tarjeta</label>
                                <input type="text" data-toggle="popover" title="Popover title" data-content="Error:{}" class="form-control onlyNumbers" onpaste="return false" name="card" id="card" placeholder="XXXX-XXXX-XXXX-XXXX" onkeypress="return solonumeros(event)" autocomplete="off" data-openpay-card="card_number" value="4111111111111111" maxlength="16">
                            </div>
                        </section>

                        <section class="col-12 d-flex flex-row flex-wrap p-0 mt-4">
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label for="xd" class="mb-3">Fecha de expiración</label>
                                <div class="d-flex">
                                    <!-- <input type="text" onpaste="return false" class="form-control onlyNumbers col-5" id="expiry_month" placeholder="Mes" onkeypress="return solonumeros(event)" data-openpay-card="expiration_month" value="" maxlength="2"> -->
                                    <select name="mes" class="form-control col-7" data-openpay-card="expiration_month" >

                                        <option value="" selected>Mes</option>
                                        <option value="01">01-Enero</option>
                                        <option value="02">02-Febrero</option>
                                        <option value="03">03-Marzo</option>
                                        <option value="04">04-Abril</option>
                                        <option value="05">05-Mayo</option>
                                        <option value="06">06-Junio</option>
                                        <option value="07">07-Julio</option>
                                        <option value="08">08-Agosto</option>
                                        <option value="09">09-Septiembre</option>
                                        <option value="10">10-Octubre</option>
                                        <option value="11">11-Noviembre</option>
                                        <option value="12">12-Diciembre</option>
                                    </select>
                                    <!-- <div class="col-2"></div> -->
                                    <!-- <input type="text" onpaste="return false" class="form-control onlyNumbers col-5" id="expiry_year" placeholder="Año" onkeypress="return solonumeros(event)" data-openpay-card="expiration_year" value="" maxlength="2"> -->
                                    <select data-openpay-card="expiration_year" maxlength="2" class="form-control col-5" data-openpay-card="expiration_month" >
                                        <option value="" selected>Año</option>
                                        <option value="19">2019</option>
                                        <option value="20">2020</option>
                                        <option value="21">2021</option>
                                        <option value="22">2022</option>
                                        <option value="23">2023</option>
                                        <option value="24">2024</option>
                                        <option value="25">2025</option>
                                        <option value="26">2026</option>
                                        <option value="27">2027</option>
                                        <option value="28">2028</option>
                                        <option value="29">2029</option>
                                        <option value="30">2030</option>
                                        <option value="31">2031</option>
                                        <option value="32">2032</option>
                                        </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label for="xd" class="mb-3">Código de seguridad</label>
                                <div class="d-flex flex-row flex-wrap p-0 justify-content-between">
                                    <div class="col-lg-5 col-md-12 p-0">
                                        <input maxlength="4" type="text" onpaste="return false" class="form-control onlyNumbers" id="cvv" placeholder="3 dígitos" onkeypress="return solonumeros(event)" autocomplete="off" data-openpay-card="cvv2" value="">
                                    </div>
                                    <div class="col-lg-7 col-md-12 text-center pt-1 pr-2 pb-0 pl-2">
                                        <img src="../openpaymodulo/assets/img/cvv_visaMastercard.png" alt="cvv mastercard" srcset="">
                                        <img src="../openpaymodulo/assets/img/cvv_amex.png" alt="cvv american express" srcset="">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="optionRefId" value="<?php echo($xd->quote->optionQuote->optionRefId)?>">
                        </section>

                        <section class="col-12 p-0 mt-4 text-right">
                        <input type="button" value="Pagar" class="btn btn-outline-success" data-toggle="modal" data-target="#modalConf" data-loading-text="Procesando..." >
                        </section>
                    </div> <!-- end body card details -->       
                    <div class="card-footer p-0 text-right">
                        <section class="col-12 d-flex flex-row flex-wrap p-0 mt-4 text-center">
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <small class="mb-2">Transacciones realizadas vía:</small> <br>
                                <img src="../openpaymodulo/assets/img/openpay.png" alt="Openpat logo" srcset="">
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-12 d-flex text-center">
                                <img class="img-responsive" src="../openpaymodulo/assets/img/security.png" alt="security logo" srcset="" style="min-width:50px;">
                                <small class="">Tus pagos se realizan de forma segura con encriptación de 256 bits</small>
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modalConf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 id="mdT" class="modal-title" id="exampleModalLabel">Confirmación de Pago!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div id="modBody" class="modal-body">
                <h6 class="text-monospace">Detalles: </h6>
                <p class="text-success">Concepto: <span class="text-black-50"><?php echo($hotelName);?></span> </p>
                <p class="text-success">Cargo: <span class="text-black-50"><?php echo($importe.' '.$currency);?> </span></p>
                <p class="text-success">Titular: <span class="text-black-50"><input class="text-black-50" id="titular2" type="text" readonly style="border:0 !important"></span></p>
                <p class="text-success">Tarjeta: <span class="text-black-50"><input class="text-black-50" id="tarjeta2" type="text" readonly style="border:0 !important"></span></p>

            </div>
            <div id="mdF" class="modal-footer bg-light">
                <button type="button" class="btn btn-success" id="pay-button"  data-whatever="confirm">Confirmar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdError" tabindex="-1" role="dialog" aria-labelledby="labelErrror" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 id="mdT" class="modal-title" id="labelErrror"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <h6 class="text-monospace">Error en los datos ingresados!</h6>
                <p class="text-danger">Mensaje: <span class="modal-error text-black-50"></span></p>
            </div>
            <div class="modal-footer bg-light">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>
 <?php
     include('../src/template/footer.php');
 ?>