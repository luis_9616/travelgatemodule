<?php
// require_once ('../src/vendor/autoload.php');//fluentPDO
require_once ('model/database.php');//fluentPDO

$controller = 'travelgate';

require_once "controller/$controller.controller.php";
$controller = ucwords($controller).'controller';
$controller = new $controller;

if (isset($_POST['checkIn']) && isset($_POST['checkOut']) && isset($_POST['rooms'])) {
    $datas = array('lang' => $_POST['langPage'], 
                   'destino' => $_POST['busqueda'],
                   'datSupplier' => $_POST['datSupplier'],
                   'checkIn' => $_POST['checkIn'], 
                   'checkOut' => $_POST['checkOut'],
                    'rooms'=>$_POST['rooms']);
                    
    $controller->searchesAllHotels($datas);
}else {
    // header('location: http://travelgatemodule.u/');
    $dates = getdate();
    if ($dates['mday'] < 10) {
        $dia = '0'.$dates['mday'];
        $diaM = $dates['mday']+2;
        if($diaM < 10){
            $diaM='0'.$diaM;
        }else{
            $diaM=$diaM;
        }
    }else{
        $dia = $dates['mday'];
        $diaM = $dates['mday']+2;
    }

    if ($dates['mon']<10) {
        $mon = '0'.$dates['mon'];
    }else{
        $mon = $dates['mon'];
    }

    $checkIn = $dates['year'] . '-' . $mon . '-'. $dia;
    $checkOut = $dates['year'] . '-' . $mon . '-' . $diaM;
    $datas = array('lang' => '/es',
    'destino' => 'Merida',
    'datSupplier' => '{"id_destiny":"1862","id_supplier":"10","sup_code":"HDO2","sup_access":"1364","leaf":"[]","closes":"[\"c#MERID\"]","usd":"19.99","eur":"21.61"}',
    'checkIn' => $checkIn,
    'checkOut' => $checkOut,
    'rooms'=>'1');
    // var_dump($datas);
    $controller->searchesAllHotels($datas);
}
