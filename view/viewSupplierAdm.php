<?php
  require_once ('model/mapping.php');
  $model;
  function __CONSTRUCT(){
      $this->model = new Mapping();
  }
?>
<script >
    $(document).on('change', '#alias', function(event) {
        $('#idSup').val($("#alias option:selected").val());
        let b = document.getElementById("idSup").value;
        window.location=`http://travelgatemodule.u/suppliersAdm.php?a=${b}`;
    });
</script>
<section class="container">
    <div class="row">
    <!-- =============================================== -->
    <!-- Menú Superior del administrador de proveerdores -->
    <!-- =============================================== -->
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" 
                   id="Supplier-tab" 
                   data-toggle="tab" 
                   href="#Supplier" 
                   role="tab" 
                   aria-controls="Supplier" 
                   aria-selected="true">Add Destinations Codes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" 
                   id="addSup-tab" 
                   data-toggle="tab" 
                   href="#addSup" 
                   role="tab" 
                   aria-controls="addSup" 
                   aria-selected="false">Add Supplier</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" 
                   id="contact-tab" 
                   data-toggle="tab" 
                   href="#contact" 
                   role="tab" 
                   aria-controls="contact" 
                   aria-selected="false">Currency</a>
            </li>
        </ul>

        <!-- ===================================================================== -->
        <!-- Cajas de contenidos para cada sección de administrador de proveedores -->
        <!-- ===================================================================== -->
        <div class="tab-content pt-3" id="myTabContent" style="width:100%">
        <?php
        // ====================================================================================
        // contenedor formulario seleccionar proveedor e iniciar el llenado de la base de datos
        // ====================================================================================
        ?>

        <div class="tab-pane fade show active" id="Supplier" role="tabpanel" aria-labelledby="Supplier-tab">
            <form class="border pt-4 pb-3 col-sm-12 col-md-12 col-lg-6" action="./destinations.php" method="post">
                    <div class="form-group">
                        <label for="alias">Supplier</label>
                        <input id="idSup" type="hidden">
                        <select name="alias" class="form-control" id="alias">
                            <option value="" selected>--Seleccionar--</option>
                            <?php
                                $xd = 'suppliers';
                                $rowSupplier = $this->model->defaultSelect($xd,'');
                                foreach ($rowSupplier as $row) {
                                    echo "<option value='".$row['id_prov']."'>".$row['alias']."</option>";
                                }
                                ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="supplier">Supplier selected</label>
                        <input type="text" class="form-control"  name="supplier" readonly value="<?php
                               if (isset($_GET['a'])) {
                                   $rowSupplier = $this->model->defaultSelect($xd,$_GET['a']);
                                //    var_dump($rowSupplier);
                                   echo($rowSupplier[0]['alias']);
                                   
                               }
                        ?>">
                    </div>
                    <div class="form-group">
                        <label for="code">Code</label>
                        <input type="text" class="form-control"  name="codeSup" readonly value="<?php
                               if (isset($_GET['a'])) {
                                $rowSupplier = $this->model->defaultSelect($xd,$_GET['a']);
                                echo($rowSupplier[0]['supplier_code']);
                                
                            }
                        ?>">
                    </div>
                    <div class="form-group">
                        <label for="accessKey">Access key</label>
                        <input type="text" class="form-control"  name="accessKey" readonly value="<?php
                               if (isset($_GET['a'])) {
                                $rowSupplier = $this->model->defaultSelect($xd,$_GET['a']);
                                echo($rowSupplier[0]['access_code']);
                                
                            }
                        ?>">
                    </div>
                    <div class="form-group">
                        <label for="range">Range</label>
                        <select name="range" class="form-control" id="range">                            
                            <option value="10">10</option>
                            <option value="100" selected>100</option>
                            <option value="1000">1000</option>
                            <option value="1000">10000</option>
                        </select>
                    </div>
                    <input type="hidden" name="id_sup" value="<?php echo($_GET['a']) ?>">
                    <div class="form-group">
                        <input type="submit" class="btn btn-outline-secondary" value="Mostrar destinos">
                    </div>
                </form>
            </div>
            <?php

                // ==================================================================================
                // contenedor formulario agregar proveedor // contenedor formulario agregar proveedor
                // ==================================================================================
            ?>
            <div class="tab-pane fade" 
                 id="addSup" 
                 role="tabpanel" 
                 aria-labelledby="addSup-tab">

                <form class="border pt-4 pb-3 col-sm-12 col-md-12 col-lg-6" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                <div class="form-group">
                    <label for="alias">Supplier</label>
                    <input type="text" class="form-control"  name="alias" placeholder="Name" required>
                </div>
                <div class="form-group">
                    <label for="region">Área</label>
                    <input type="text" class="form-control"  name="region" placeholder="America" required>
                </div>
                <div class="form-group">
                    <label for="code">Code</label>
                    <input type="text" class="form-control"  name="codeSup" placeholder="Code (HDO)" required>
                </div>
                <div class="form-group">
                    <label for="accessKey">Access key</label>
                    <input type="text" class="form-control"  name="accessKey" placeholder="Access (1364)" required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-outline-info" name="supplierDatas" value="Guardar">
                </div>
            </form>
            </div>
            <?php
            
                // ===========================================================
                // contenedor formulario para actualizar los cambios de moneda
                // ===========================================================
            ?>
            <div class="tab-pane fade" 
                 id="contact" 
                 role="tabpanel" 
                 aria-labelledby="contact-tab">
                
                <form class="border pt-4 pb-3 col-sm-12 col-md-12 col-lg-6" action=".php" method="post">
                    <article class="col-sm-12 col-md-12 col-lg-12 d-flex flex-row justify-content-center align-items-center">
                        <div class="form-group col-sm-12 col-md-12 col-lg-4">
                            <label for="inputUSD"><i class="fas fa-hand-holding-usd"></i> USD a MXN</label>
                        </div>
                        <?php
                            $row = $this->model->defaultSelect3('trvl_currency_change','1', 'id_currency');
                        ?>
                        <div class="form-group col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control" id="inputUSD" placeholder="" value="<?php echo $row[0]['USD'] ?>" disabled>
                        </div>
                    </article>
                    
                    <article class="col-sm-12 col-md-12 col-lg-12 d-flex flex-row justify-content-center align-items-center">
                        <div class="form-group col-sm-12 col-md-12 col-lg-4">
                            <label for="inputEUR" class=""><i class="fas fa-euro-sign"></i> EUR a MXN</label>
                        </div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control" id="inputEUR" placeholder="" value="<?php echo $row[0]['EUR'] ?>"  disabled>
                        </div>
                    </article>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12 mt-4">
                        <button type="submit" class="btn btn-primary btn-block btn-lg mb-2">Actualizar</button>
                    </div>
                </form>
            </div>
    </div>

<?php

    if(isset($_POST['supplierDatas'])){

        if (isset($_POST['accessKey'])) {
            $accessCode = strtoupper($_POST['accessKey']);

        }if (isset($_POST['codeSup'])) {
            $supplierCode = $_POST['codeSup'];

        }if (isset($_POST['alias'])) {
            $alias = $_POST['alias'];

        }if (isset($_POST['region'])) {
            $region = $_POST['region'];
        }else{
            echo(' no hay datos');
        }
        $values = array('access_code' => $accessCode, 'supplier_code'=>$supplierCode, 'alias'=>$alias, 'region' => $region);
        // var_dump($values);
        $supplier = $this->model->insertSupplier($values);
        // print_r($supplier);
        echo('El proveerdor con código de acceso: <b>'.$supplier[0]['access_code'].'</b> ya existe en la base de datos. ');
        // var_dump($supplier);
        
    }else {}
    if (isset($_GET['a'])) {
        echo($_GET['a']);
    }else{
        echo '';
    }