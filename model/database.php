<?php
    class Database{
        public static function StartUp(){
            $pdo = new PDO('mysql:host=localhost;dbname=db_travelgate;charset=utf8','root','root1234');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            return $pdo;
        }
    }
?>