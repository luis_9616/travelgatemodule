<?php
require_once __DIR__ . '/../vendor/autoload.php';
include('./config.php');

// use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;

// var_dump($paxes);

//query body
$lorem = 'query {
  hotelX {
    hotels(criteria: {
    access: "1364",
    maxSize: 100,
    hotelCodes: ["1859"]
  },
  relay: {}) {
      token
      edges {
        node {
          code
          hotelData {
            code
            hotelCode
            hotelCodeSupplier
            hotelName
            categoryCode
            rank
            cardTypes
            contact {
              email
              telephone
              fax
              web
            }
            property {
              name
              code
            }
            chainCode
            exclusiveDeal
            rank
            cardTypes
            location {
              address
              city
              zipCode
              country
              coordinates {
                latitude
                longitude
              }
              closestDestination {
                code
                available
                destinationLeaf
                parent
                type
                texts {
                  text
                  language
                }
              }
            }
            amenities {
              code
              type
              texts {
                text
                language
              }
            }
            medias {
              code
              order
              type
              updatedAt
              url
              texts {
                text
                language
              }
            }
            descriptions {
              type
              texts {
                text
                language
              }
            }
          }
          error {
            code
            type
            description
          }
          createdAt
          updatedAt
        }
      }
    }
  }
}';

// Create the GraphQL query
$gql = <<<QUERY
$lorem
QUERY;

try {
    $results = $client->runRawQuery($gql);
}

catch (QueryError $exception) {
    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}


// Display part of the returned results of the object
// var_dump($results->getData()->hotelX);
$xd = $results->getData()->hotelX;
// var_dump($xd);
$title = 'Static Data';
include('../src/template/header.php');
print_r('lorem: ');
print_r($xd->hotels->edges[0]->node->hotelData->medias[0]->url);
?>
  <section class="container">
    <div class="row">
      <div class="col d-flex flex-row flex-wrap justify-content-between align-items-center">
    
      <?php
       if($xd->hotels->edges!==null){
          for ($i=0; $i < count($xd->hotels->edges) ; $i++) { 
      ?>
        <form class="card col-sm-12 col-md-6 col-lg-6" action="./query_quote.php" method="post">
          <?php
                for ($i=0; $i <count($xd->hotels->edges[0]->node->hotelData->medias); $i++) { 
                    ?>
                    <img src="<?php 
          print_r($xd->hotels->edges[0]->node->hotelData->medias[$i]->url);
          ?>" class="card-img-top" alt="...">
                    <?php
                }
          ?>
          <div class="card-body">
            <h5 class="card-title">
            <?php 
            
            ?><br>
            <input type="text" name="hotelName" id="hotelName" value=" <?php 
            
            ?>" style="border:0px;" readonly>
            </h5>     
            <p class="card-text">
              Precio: <span class="card-text text-black-50">
              <?php
            
              ?>
              </span>
              Bruto: <span class="card-text text-black-50">
              <?php
            
              ?>
              </span>
            </p>
            <p class="card-title text-capitalize">
            <span class="text-secondary text-success">Detalles:</span> <br>
            Habitaciones disponibles: <?php 
            
            ?> </p>
            <?php
                  for ($i2=1; $i2 <= count($xd->search->options[$i]->occupancies) ; $i2++) { 
                    ?>
                    <p class="card-title text-capitalize text-info">
                       Descripción de la Habitación <?php echo($i2) ?>
                    </p>
                    <p class="card-text text-black-50">
                      <span class="card-text text-dark">Código:</span> <?php echo($xd->search->options[$i]->rooms[$i2-1]->code) ?>
                    </p>
                    <p class="card-text text-black-50">
                      <span class="card-text text-dark">Descripción:</span> <?php echo($xd->search->options[$i]->rooms[$i2-1]->description) ?>
                    </p>
                    <p class="card-text text-black-50">
                      <span class="card-text text-dark">Precio:</span> <?php echo($xd->search->options[$i]->rooms[$i2-1]->roomPrice->price->net) ?>,
                      <span class="card-text text-dark">Bruto:</span> <?php echo($xd->search->options[$i]->rooms[$i2-1]->roomPrice->price->gross) ?>
                    </p>
                    <!-- seccion de beds -->
                      <?php
                      for ($i3=1; $i3 <= count($xd->search->options[$i]->rooms[$i2-1]->beds) ; $i3++) {
                      ?>
                        <p class="card-text text-black-50">
                          <span class="card-text text-dark">Tipo:</span> <?php 
                          print_r($xd->search->options[$i]->rooms[$i2-1]->beds[$i3-1]->type) 
                          ?>,
                          <span class="card-text text-dark">Compartido:</span> <?php 
                          if ($xd->search->options[$i]->rooms[$i2-1]->beds[0]->shared == false) {
                            echo("false");
                            }else {
                              echo("true");
                          }
                      ?>
                        </p>
                    <?php
                    }
                    ?>
                    </p>
                  <?php
                }
            ?>
            <input type="hidden" name="optionRefId" value="<?php echo($xd->search->options[$i]->id)?>">
            <!-- </p> -->
            
            <input class="btn btn-outline-success" type="submit" value="Seleccionar">
          </div>
        </form>
<?php
    }
}else{
  echo('no hay coincidencias con su busqueda');
}
?>
      </div> 
    </div>
  </section>
  <?php
    include('../src/template/footer.php');
  ?>