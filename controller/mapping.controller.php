<?php
require_once 'model/mapping.php';
require_once 'model/travelgate.php';

class mappingController{
    private $model;
    private $model2;

    public function __CONSTRUCT(){
        $this->model = new Mapping();
        $this->model2 = new Travelgate();
    }
    public function Index(){
        $title = 'Home';
        // $client = $this->model->getClient();
        require_once 'view/header.php';
        require_once 'view/home.php';
        require_once 'view/footer.php';
    }
    public function viewCountries(){
        $title = 'Insert Countries';        
        require_once 'view/header.php';
        require_once 'view/ViewCountry.php';
        require_once 'view/footer.php';
    }
    public function viewAdminSuppliers(){
        $title = 'Admin Suppliers';        
        require_once 'view/header.php';
        require_once 'view/viewSupplierAdm.php';
        require_once 'view/footer.php';
        
    }
    public function viewAdminDestinations(){
        $title = 'Admin destinations';        
        require_once 'view/header.php';
        require_once 'view/viewSupplierAdm.php';
        require_once 'view/footer.php';
    }
    public function destinations($tokenDestinations, $sizeDestinations, $accessSupplier, $id_sup){
        $title='Get Destinations';
        $destiny1 = $this->model2->getDestinations($tokenDestinations, $sizeDestinations, $accessSupplier);
        $id_sup = $id_sup;
        require_once ('view/header.php');
        require_once ('view/viewDestinations.php');
        require_once ('view/footer.php');
    }
    public function insertHotelMedias(){
        $title = "Hotel medias uploading...";
        $medias = $this->model->defaultSelect("tmp_hcodes","");
        require_once 'view/header.php';
        require_once 'view/viewHotelMedias.php';
        require_once 'view/footer.php';
    }
    public function joint(){
        $title = "Hong...";
        // $medias = $this->model->defaultSelect("hotels","");
        $medias = $this->model->joins('762');
        require_once 'view/header.php';
        require_once 'view/viewJoin.php';
        require_once 'view/footer.php';
    }
    public function getCurrency(){
    // ==============================
    // comversion de cambio de moneda 
    // ==============================
    $datas = array('USD' => $this->model->getCurrency(1, 'USD', 'MXN') , 'EUR' => $this->model->getCurrency(1, 'EUR', 'MXN'), 'updated_at' => date('Y-m-d H:i:s'));
    $insert = $this->model->updateCurrency('trvl_currency_change', $datas, 'id_currency', 1);
    var_dump($datas);
    }
}
?>