<section class="container">
  <div class="row">
    <div class="col-10 p-4 border m-auto">
  <?php
    // =================================================
    // Variable auxilar para tener el id del proveedor
    $aux_sup = $id_sup;

    // =================================================
    // Cuenta los elementos de array que devuelve la consulta
    // =================================================
    $edges = $destiny1->destinations->edges;
    echo( 'Total de resultados: '. count($edges) . '<br>');    
    
    // ================================================================================
    // if del ciclo general que recorre cada uno des los elementos de array principal
    // ================================================================================
      if ($edges[0]->node->destinationData !=null) {// if del ciclo general
        for ($i=0; $i < count($edges); $i++){

          // =============================================
          // verifica la disponibilidad de cada resultado
          // =============================================
          if ($edges[$i]->node->destinationData->available == true) {
            // echo('Disponible');
            $ava = 'Disponible';
          }else {
            $ava = 'No disponible';
            // echo('No disponible');
          }

          // ==========================
          // Arreglos para los destinos 
          // ========================== 
          $arrDestLeaf = array();
          $arrCloDest = array();
          
          // ===========================================================
          // ciclo que comprueba los texto en el idioma español e inglés
          // ===========================================================
          for ($z=0; $z  < count($edges[$i]->node->destinationData->texts) ; $z++) {
            
            if ($edges[$i]->node->destinationData->texts[$z]->language == 'en') {
              // echo(($edges[$i]->node->destinationData->texts[$z]->text));
              // echo('<br>');
              $textEn = ($edges[$i]->node->destinationData->texts[$z]->text);
            }else{
              $textEn = 'language not available';
            }

            if ($edges[$i]->node->destinationData->texts[$z]->language == 'es') {
              // echo(($edges[$i]->node->destinationData->texts[$z]->text));// echo('<br>');
              // echo('<br>');
              $textEs = ($edges[$i]->node->destinationData->texts[$z]->text);
            }else{
              $textEs = 'idioma no disponible';
            }
            $textDef = $edges[$i]->node->destinationData->texts[0]->text;
          }
          
          // ====================================
          // verificacion de los destination leaf
          // ====================================
          if (isset($edges[$i]->node->destinationData->destinationLeaf)) {
            for ($i1=0; $i1 <count($edges[$i]->node->destinationData->destinationLeaf); $i1++) {
              // echo(($edges[$i]->node->destinationData->destinationLeaf[$i1]));
              // echo('<br>');
              array_push($arrDestLeaf,($edges[$i]->node->destinationData->destinationLeaf[$i1]));
            }
          }else {
            echo('');
          }
          // =======================================
          // verificacion de los closes destinations
          // =======================================
          if (isset($edges[$i]->node->destinationData->closestDestinations)) {
            for ($i2=0; $i2 <count($edges[$i]->node->destinationData->closestDestinations); $i2++) {
              // echo(($edges[$i]->node->destinationData->closestDestinations[$i2]));
              // echo('<br>');
              array_push($arrCloDest,($edges[$i]->node->destinationData->closestDestinations[$i2]));
            }
            // echo('Array con los closes destinations: <br>');var_dump($arrCloDest);
          }else{
            echo '';
          }

          // ========================================================================
          // despues de comprobar y obtener los datos se procede a insertar los datos
          // ========================================================================
          $codeDest = ($edges[$i]->node->destinationData->code);

          
          echo('<br>Test para comprabar destinos en la base de datos: ');
          $testExist = $this->model->verifyDestiny($aux_sup, $codeDest);
          // var_dump($testExist);

          if (empty($testExist)) {
          echo 'Los datos no existen, entonces se insertan a la base de datos <br><br>';
            if($edges[$i]->node->destinationData->parent != null){
              $auxParent = $edges[$i]->node->destinationData->parent;
            }else {
              $auxParent = 'No parent';
            }

          // ============================
          // array para insertar destinos
          // ============================
          $datasDest = array('id_supplier' =>intval($aux_sup),
                             'destination_code'=> ($edges[$i]->node->destinationData->code),
                             'available'=>$ava,
                             'parent'=>($auxParent),
                             'type'=>($edges[$i]->node->destinationData->type),
                             'city_es'=>$textEs,          
                             'city_en'=>$textEn,
                             'city_default'=>$textDef);
              
          // echo('Array con los datos de destino para insertar: ');
          // var_dump($datasDest);
          
          // ==============================
          // query que inserta los destinos 
          // ==============================
          $queryDest = $this->model->destinyBySupplier($datasDest); //query que inserta los destinos 
          // var_dump($queryDest);
            echo '<br>';
            echo 'Destino número: '.($queryDest);
            echo '<br>';
          // sleep(1);
          
          // =======================================================================================
          // array que contine los códigos de destinos por cada resultado en la consulta de destinos
          // =======================================================================================
            $datasCodDes = array('id_destinations'=> intval($queryDest), 
            'destination_leaf'=>json_encode($arrDestLeaf),
            'closes_destinations'=>json_encode($arrCloDest));
            // echo('Array con los codigos de destinos por cada resultado: <br>'); 
            // var_dump($datasCodDes);
            	// echo('<br>');
             if (!empty($queryDest)) {
               // query que inserta los codigos de destino  // query que inserta los codigos de destino
               $queryCodeDest = $this->model->codeDestSupplier($datasCodDes); 
              }else{
                echo('<br><br> Códigos de destinos no insertados revisa el código<br><br>');
              }
              echo('<br>Total actual de datos insertados: '.$queryDest);
              echo('<br>');

          }else{
            echo('<br><br>');
            echo('"Los datos ya existen!"');
            echo('<br><br>');
          }
          sleep(1);
        }
      }// fin del if general
      else{
        // var_dump($edges[0]->node->error->);
        echo '<br>Si esta es la primera iteración el proveedor no devuelve resultado, sino, se termino de procesar los destino que ofrece el proveedor: <br>';
        echo('Código de error: ');
        echo($edges[0]->node->error[0]->code);        
        echo('<br> Msg de error: ');
        echo($edges[0]->node->error[0]->description);
        echo('<br>');
      }
      // ===================================================================
      // Variable auxilar para capturar el primer token que trae la consulta
      // ===================================================================
      $auxToken = ($destiny1->destinations->token);

$count2 = 3;
// ========================================
// for que hace con el trabajo de verificar
// ========================================
 for ($count=2; $count < $count2; $count++) {
      $auxCount = $count-1;
      echo('<br>');
      echo('<br>');
      echo('<br>');
// ====================================
// comprueba que el token no este vacio
// ====================================
  if ((${"destiny".$auxCount})->destinations->token != null) {
    ${"token".$auxCount} = ${"destiny".$auxCount}->destinations->token;
    echo('verificacion del token');
    echo('<br>');
    var_dump(${"token".$auxCount});
    echo('<br>');
    ${"destiny".$count} = $this->model2->getDestinations(${"token".$auxCount}, $sizeDestinations, $accessSupplier);
    echo('<br>');
    echo('<br>');
    // echo('<pre>');
    // var_dump(${"destiny".$count});
    // echo('</pre>');
    echo('<br>');
    echo('<br>');
    
    if (${"destiny".$count}->destinations->edges[0]->node->destinationData != null) {
      // =============================
      // for que recorre todo el array
      // =============================
      for ($ic=0; $ic < count(${"destiny".$count}->destinations->edges); $ic++){
        // echo(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->code);      // echo(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->parent);      // echo(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->type);
        if (${"destiny".$count}->destinations->edges[$ic]->node->destinationData->available == true) {
          // echo('Disponible');
          ${"ava".$count} = 'Disponible';
        }else {
          ${"ava".$count} = 'No disponible';
          // echo('No disponible');
        }
        for ($z1=0; $z1  < count(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->texts) ; $z1++) {
          
          if (${"destiny".$count}->destinations->edges[$ic]->node->destinationData->texts[$z1]->language == 'en') {
            echo((${"destiny".$count}->destinations->edges[$ic]->node->destinationData->texts[$z1]->text));
            ${"textEn".$count} = (${"destiny".$count}->destinations->edges[$ic]->node->destinationData->texts[$z1]->text);
          }else{
            ${"textEn".$count} = 'language not available';
          }

          if (${"destiny".$count}->destinations->edges[$ic]->node->destinationData->texts[$z1]->language == 'es') {
            echo((${"destiny".$count}->destinations->edges[$ic]->node->destinationData->texts[$z1]->text));
            ${"textEs".$count} = (${"destiny".$count}->destinations->edges[$ic]->node->destinationData->texts[$z1]->text);
          }else{
            ${"textEs".$count} = 'idioma no disponible';
          }
          
          ${"textDef".$count} = (${"destiny".$count}->destinations->edges[$ic]->node->destinationData->texts[0]->text);
        }      
        ${"arrDestLeaf".$count} = array();
        ${"arrCloDest".$count} = array();
        
        // ====================================
        // verificacion de los destinos parte 1
        // ====================================
        if (isset(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->destinationLeaf)) {
          for ($i2=0; $i2 <count(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->destinationLeaf); $i2++) {
            // echo((${"destiny".$count}->destinations->edges[$ic]->node->destinationData->destinationLeaf[$i2]));
            array_push(${"arrDestLeaf".$count},(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->destinationLeaf[$i2]));
          }
          // echo('Array con los destinations leaf: ');
          // var_dump(${"arrDestLeaf".$count});
        }else {
          echo('');
        }
        // ====================================
        // verificacion de los destinos parte 2
        // ====================================
        if (isset(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->closestDestinations)) {
          for ($i3=0; $i3 <count(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->closestDestinations); $i3++) {
            // echo((${"destiny".$count}->destinations->edges[$ic]->node->destinationData->closestDestinations[$i3]));
            array_push(${"arrCloDest".$count},(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->closestDestinations[$i3]));        
          }
            // echo('Array con los closes destinations: ');          // var_dump(${"arrCloDest".$count});
        }else{
          echo '';
        }
        
      // ==========================================================================
      // ==========================================================================
      // Ciclo para la insercion de los datos si estos no están en la base de datos
      // ==========================================================================
      // ==========================================================================
      ${"codeDest".$count} = ${"destiny".$count}->destinations->edges[$ic]->node->destinationData->code;
        //cambiar este parte genera un error
            echo('<br> Test para comprabar destinos en la base de datos: ');
            ${"testExist".$count} = $this->model->verifyDestiny($aux_sup, ${"codeDest".$count});
            // var_dump(${"testExist".$count});
            
            if(empty(${"testExist".$count})){
              echo 'Los datos no existen, entonces se insertan a la base de datos <br><br>';
              // ===================================================
              // Comprobar si exite el parent
              // ===================================================
              if(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->parent != null){
                ${"auxParent".$count} = ${"destiny".$count}->destinations->edges[$ic]->node->destinationData->parent;
              }else {
                ${"auxParent".$count} = 'No parent';
              }
              //array para insertar destinos //array para insertar destinos
              ${"datasDest".$count} = array('id_supplier' =>intval($aux_sup),
                                            'destination_code'=> (${"destiny".$count}->destinations->edges[$ic]->node->destinationData->code),
                                            'available'=>${"ava".$count},
                                            'parent'=>${"auxParent".$count},
                                            'type'=>(${"destiny".$count}->destinations->edges[$ic]->node->destinationData->type),
                                            'city_es'=>${"textEs".$count},
                                            'city_en'=>${"textEn".$count},
                                            'city_default'=>${"textDef".$count});
            
            // echo('Arrar con los datos de destino para insertar: ');
            // echo('Resutado de la inserción de los destinos');
            // var_dump(${"datasDest".$count});
            
        // ===================================================================
        // query para insertar los datos de destino por cada porveedor
        // ===================================================================
        ${"queryDest".$count} = $this->model->destinyBySupplier(${"datasDest".$count});      
        echo '<br>Destino número: ' . (${"queryDest".$count});
        echo '<br>';
        // sleep(1);

        // ===================================================================
        //array que contine los códigos de destinos por cada resultado en la consulta de destinos
        // ===================================================================
        ${"datasCodDes".$count} = array('id_destinations'=> intval(${"queryDest".$count}),
                                        'destination_leaf'=>json_encode(${"arrDestLeaf".$count}),
                                        'closes_destinations'=>json_encode(${"arrCloDest".$count}));

        // =============================================================
        // QUERY que inserta los codigos de destino
        // =============================================================
        if (!empty(${"queryDest".$count})) {
          ${"queryCodeDest".$count} = $this->model->codeDestSupplier(${"datasCodDes".$count});
        }else{
          echo('<br><br>No se insetaron códigos de destino revisa el código<br><br>');
        }

        echo('<br>Total de datos insertados: '.${"queryDest".$count});
        // sleep(1);
        echo('<br>');
        echo('<br>');
      }else{
        echo('<br><br>');
        echo('Los datos ya existen!');
        echo('<br><br>');
      }
      sleep(1);
    }
    }else {
        //variable a inmprimir
        echo '<br>Si esta es la primera iteración el proveedor no devuelve resultado, sino, se termino de procesar los destino que ofrece el proveedor: <br>';
        echo 'Código de error: ';
            echo(${"destiny".$count}->destinations->edges[0]->node->error[0]->code);
            echo('<br> Msg de error: ');
            echo(${"destiny".$count}->destinations->edges[0]->node->error[0]->description);
            echo('<br>');
    }

   }
   if ((${"destiny".$auxCount})->destinations->token != null) {
     $count2=$count2+1;
   }else {
     $count2=$count2;
   }
 }
// ejemplo de post en la misma pagina
// if(isset($_POST['xd'])){
//   $name = $_POST['supplierCode'];
//   echo "User Has submitted the form and entered this name : <b> $name </b>";
//   echo "<br>You can use the following form again to enter a new name.";
// }
?>
      </div>
    </div>
</section>