<?php
//fluentPDO
 require_once __DIR__ .'/../src/vendor/autoload.php';

//call library of Graphql/client for mostafa
 require_once __DIR__ . '/../vendor/autoload.php';

 // use GraphQL\Client;
 use GraphQL\Exception\QueryError;
 use GraphQL\Client;
 use GraphQL\Query;

    class Mapping{
      // ===============================
      // variables for all the functions
      // ===============================
        private $pdo;
        private $fluent;

        // ================================
        // constructor for the bd conection
        // ================================        
        public function __CONSTRUCT(){
            try {
                $this->pdo = Database::StartUp();
                $this->fluent = new \Envms\FluentPDO\Query($this->pdo);
            } catch (\Throwable $th) {
               die($th->getMessage());
            }
        }
        
        // =====================================================================================
        // función para insertar los países para guardar los códigos de región según la ISO 3166
        // =====================================================================================
        public function insertCountry($datas){
            try {
                
                // $query = $fluent->insertInto('article')->values($values)->execute();
                // $query = $fluent->insertInto('article', $values)->execute(); // shorter version
                $values = $datas;
                // $fluent = new \Envms\FluentPDO\Query($this->pdo);
                $query = $this->fluent->insertInto('country_codes')
                                ->values($values)
                                ->execute();
                                // echo('se insertaron: '); // var_dump($query); // echo('<br>');
                                return $query;
            } catch (\Throwable $th) {
                echo('error');
                var_dump($th);
            }
        }

        // =========================================================
        // función para insertar los proveedorres a la base de datos
        // =========================================================
        public function insertSupplier($values){
            try {
                $verify = $this->verifySupplier($values['access_code']);
                if (empty($verify)) {
                    $query = $this->fluent->insertInto('suppliers')
                                    ->values($values)
                                    ->execute();
                    return $query;
                }else{
                    // var_dump($verify);
                    return $verify;
                }
                
            } catch (\Throwable $th) {
                echo('error');
                var_dump($th);
            }
        }
        
        // ====================================================================
        // función para verificar si el proveedor ya existe en la base de datos
        // ====================================================================
        public function verifySupplier($supplier){
            try {
                $sql = $this->fluent->from('suppliers')->where('access_code', $supplier);
                // foreach ($sql as $row) {
                //     echo "$row[alias]\n";
                // }
                $res = $sql->fetchAll();
                return $res;
            } catch (\Throwable $th) {
                //throw $th;
                var_dump($th);
            }
        }
        
        // ==============================================
        // función para verificar si el destino ya existe
        // ==============================================
        public function verifyDestiny($supplier, $codeDestiny){
            try {
                $sql = $this->fluent->from('detinations_by_suppliers')->where('id_supplier', $supplier)->where('destination_code',$codeDestiny);
                $res = $sql->fetchAll();
                return $res;
            } catch (\Throwable $th) {
                //throw $th;
                echo('Error al comprobar los destinos');
                var_dump($th);
            }
        }
        
        // ===============================================================
        // función para verificar si existen los datos en la base de datos
        // ===============================================================
        public function verifyDatas($table, $row1, $supplier, $row2, $code){
            try {
                $sql = $this->fluent->from($table)
                                    ->where($row1, $supplier)
                                    ->where($row2, $code);
                $res = $sql->fetchAll();
                return $res;
            } catch (\Throwable $th) {
                //throw $th;
                echo('Error al verificar los datos');
                var_dump($th);
            }
        }
        
        // =======================================================
        // función para verificar si el codigo del hotel ya existe
        // =======================================================
        public function verifyHCodes($code,$access){
            try {
                $sql = $this->fluent->from('tmp_hcodes')->where('codes', $code)->where('access_sup', $access);
                $res = $sql->fetchAll();
                return $res;
            } catch (\Throwable $th) {
                //throw $th;
                echo('Error al comprobar los destinos revisar funcion verifyHCodes en mapping');
            }
        }
        
        // ================================================
        // función para consulta general a la base de datos
        // ================================================
        public function defaultSelect($table, $id){
            try {
                if ($id == '') {
                    $sql = $this->fluent->from($table);
                    return $sql;
                }else {
                    $sql = $this->fluent->from($table)->where('id_prov', $id);
                    $res = $sql->fetchAll();
                    return $res;
                }
            } catch (\Throwable $th) {
                //throw $th;
                var_dump($th);
            }
        }
        // ================================================
        // función para consulta general a la base de datos
        // ================================================
        public function defaultSelect2($table, $rows, $idz){
            try {
                    $sql = $this->fluent->from($table)->where($rows, $idz);
                    $res = $sql->fetchAll();
                    return $res;
            } catch (\Throwable $th) {
                var_dump($th->getMessage());
            }
        }

        // ================================================
        // función para consulta general a la base de datos
        // ================================================
        public function defaultSelect3($table, $id, $row){
            try {
                if ($id == '') {
                    $sql = $this->fluent->from($table);
                    return $sql;
                }else {
                    $sql = $this->fluent->from($table)->where($row, $id);
                    $res = $sql->fetchAll();
                    return $res;
                }
            } catch (\Throwable $th) {
                //throw $th;
                var_dump($th);
            }
        }
        // ============================================================
        // función para insertar los destinos que ofrece cada proveedor
        // ============================================================
        public function destinyBySupplier($datas){
            try {
                
                $query = $this->fluent->insertInto('detinations_by_suppliers')
                                      ->values($datas)
                                      ->execute();
                                      return $query;
            } catch (\Throwable $th) {
                echo('error al insertar detinations_by_suppliers: ');
                echo('<pre>');
                    var_dump($th);
                echo('</pre>');
            }
        }

        // ======================================================
        // función para insertar datos a la base de datos
        // ======================================================
        public function insertDataGrnl($table, $codes,$access){
            try {
                $code = array('codes' => $codes,'access_sup'=>$access);
                $query = $this->fluent->insertInto($table)
                                      ->values($code)
                                      ->execute();
                                      return $query;
            } catch (\Throwable $th) {
                echo('error al insertar los datos desde la query general: ');
            }
        }

        // ======================================================
        // función general para insertar datos a la base de datos
        // ======================================================
        public function insertAnyDatas($table, $datas){
            try {
                $query = $this->fluent->insertInto($table)
                                      ->values($datas)
                                      ->execute();
                                      return $query;
            } catch (\Throwable $th) {
                echo('error al insertar los datos (insertAnyDatas)');
                	echo('<pre>');
                    //variable a inmprimir
                        var_dump($th);
                    echo('</pre>');
            }
        }

        // ======================================================
        
        // ======================================================
        function array_push_assoc(array &$arrayDatos, array $values){
            $arrayDatos = array_merge($arrayDatos, $values);
            return $arrayDatos;
        }
        
        // ===========================================================
        // función para insertar los códigos de destinos por proveedor
        // ===========================================================
        public function codeDestSupplier($datas){
            try {
                $query = $this->fluent->insertInto('codes_destinations')
                ->values($datas)
                ->execute();
                return $query;
            } catch (\Throwable $th) {                
                echo('error al insertar los códigos de destinos: ');
                	echo('<pre>');
                        // var_dump($th);
                    echo('</pre>');
            }
        }
        
        // ===============================================================================
        // función para insertar los cambios de monedas en 3 tipos de cambio MXN, USD, EUR
        // ===============================================================================
        public function currency_change($datas){
            try {                
                $data = array('coin' => $datas['MXN']);
                $query = $this->fluent->update('currency_change')
                                ->set($data)
                                ->where('id_currency', '')
                                ->execute();

                return $query;
            } catch (\Throwable $th) {                
                echo('error al actualizar los cambios de monedas: ');
                echo($th->getMessage());
            }
        }

        // ===============================================================================
        // función para 
        // ===============================================================================
        public function joins($hotel){
            try {                
                $query="SELECT Ho.id_hotel, Ho.hotel_code,
							   Detail.category,
							   Media.all_medias
						FROM hotels Ho
						INNER JOIN hotel_details Detail ON Ho.id_hotel = Detail.id_hotel
                        INNER JOIN hotel_medias Media ON Ho.id_hotel = Media.id_hotel
                        WHERE Ho.hotel_code = $hotel";
                $c=$this->pdo->query($query);

                while($f=$c->fetch(PDO::FETCH_ASSOC)){
                    $data = $f;
                }
                return $data;
            } catch (\Throwable $th) {                
                echo('error en el join: <br>');
                // echo($th->getMessage());
                	echo('<pre>');
                    //variable a inmprimir
                        var_dump($th->getMessage());
                    echo('</pre>');
            }
        }
        
        // =======================================================
        // función para verificar si el codigo del hotel ya existe
        // =======================================================
        public function verifyExist($table, $col, $data){
            try {
                $sql = $this->fluent->from($table)->where($col, $data);
                $res = $sql->fetchAll();
                return $res;
            } catch (\Throwable $th) {
                //throw $th;
                echo('Error al comprobar los destinos revisar funcion verifyExist en mapping');
            }
        }

        // =============================
        // función para borrar registros
        // =============================
        public function deleteAny($table, $id, $col){
            try {
                $sql = $this->fluent->deleteFrom($table)
                            ->where($col, $id)
                            ->execute();
                return $sql;
            } catch (\Throwable $th) {
                //throw $th;
                echo('Error al comprobar los destinos revisar funcion verifyExist en mapping');
            }
        }

        // =======================================================
        // función para verificar si el codigo del hotel ya existe en hotels
        // =======================================================
        public function verifyHo($table,$code,$access){
            try {
                $sql = $this->fluent->from($table)->where('hotel_code', $code)->where('supplier_access', $access);
                $res = $sql->fetchAll();
                return $res;
            } catch (\Throwable $th) {
                //throw $th;
                echo('Error al comprobar los destinos revisar funcion verifyHCodes en mapping');
            }
        }
        
        // ===========================================
        // función para insertar los cambios de moneda
        // ===========================================
        public function getCurrency($amount,$from_currency,$to_currency){
            try {
                    $apikey = '413f3b526c7e902f4a20';
                  
                    $from_Currency = urlencode($from_currency);
                    $to_Currency = urlencode($to_currency);
                  
                    $query =  "{$from_Currency}_{$to_Currency}";
                  
                    $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
                    $obj = json_decode($json, true);
                  
                    $val = floatval($obj["$query"]);
                  
                    $total = $val * $amount;
                    return number_format($total, 2, '.', '');

            } catch (\Throwable $th) {
                echo('Error al insertar los cambios de moneda');
            }
        }
        
        // ===========================================
        // función para insertar los cambios de moneda
        // ===========================================
        public function updateCurrency($table, $datas, $col, $id){
            try {
                $query = $this->fluent->update($table)
                                      ->set($datas)
                                      ->where($col, $id)
                                      ->execute();
                return $query;
            } catch (\Throwable $th) {
                echo('Error al insertar los cambios de moneda');
            }
        }
    }