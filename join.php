<?php
require_once ('model/database.php');
require_once ('./src/vendor/autoload.php');//fluentPDO
$controller = 'mapping';

require_once "controller/$controller.controller.php";
$controller = ucwords($controller).'controller';
$controller = new $controller;

// try {
//     $pdo = Database::StartUp();
//     $fluent = new \Envms\FluentPDO\Query($pdo);
// } catch (\Throwable $th) {
//    die($th->getMessage());
// }
$controller->joint();
?>