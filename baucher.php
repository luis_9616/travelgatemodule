<?php

require_once("src/dompdf/autoload.inc.php");
require_once("src/dompdf/lib/html5lib/Parser.php");
require_once("src/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php");
require_once("src/dompdf/lib/php-svg-lib/src/autoload.php");
require_once("src/dompdf/src/Autoloader.php");
Dompdf\Autoloader::register();

$baucher = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Baucher</title>
    <link rel="stylesheet" href="src/css/style.css">
    <link rel="stylesheet" href="src/css/bootstrap.css">
</head>
<body>
    <div class="container">
        <div class="row bg-baucher">
            <img id="bg-mem" src="src/img/bg-mem.png" alt="">
            <div id="all-text" class="w-100">
                <!-- titulo -->
                <h2 class="title text-center text-success">HOTEL SERVICES E-VOUCHER</h2>
                <div class="confirmation">
                    <div class="confirm-number bg-success text-white text-center">
                    CONFIRMATION: <br> Nº P1309418
                    </div>
                    <div class="release">
                        <p>Hotel Name</p>
                        <p>Hotel address</p>
                        <p><span class="text-success">Guest (s):</span> Chantelle Roelofse, Song Jing.</p>
                        <p><span class="text-success">Check In:</span> 08/25/2019.</p>
                        <p><span class="text-success">Check Out:</span> 08/30/2019.</p>
                    </div>
                </div>
                <br> <br>
                <div class="resumen">
                    <div class="box">
                        <div class="boxTop">ROOM(S)</div>
                        <div class="boxBot bg-success">1</div>
                    </div>
                    <div id="box02" class="box">
                        <div class="boxTop">ROOM(S) TYPE</div>
                        <div class="boxBot bg-success">SPECIAL</div>
                    </div>
                    <div id="box03" class="box">
                        <div class="boxTop">ADULT(S)</div>
                        <div class="boxBot bg-success">2</div>
                    </div>
                    
                </div>
                <br> <br>
                <br> <br> <br>
                <div class="notes">
                    <h3 id="polices" class="text-success">ADITIONAL NOTES:</h3>
                    <h3 class="text-success">POLICIES:</h3>
                    <ol>
                        <li>This Coupon will not be valid if it does not present CONFIRMATION KEY</li>
                        <li>Documents assessed only for the specified and prepaid services that have been specified</li>
                        <li>All confirmed reservations will present charges for cancellations depending on the hotel or service paid.</li>
                        <li>Any reproduction of this coupon not authorized by Go Tour will cause its invalidity.</li>
                        <li>For reimbursement of early departures request the hotel proof of authorization (this will be according to the policies of each hotel) in case of not having it, NO refund will proceed.*</li>
                        <li>In case of any doubt, comment or clarification please contact our agents at the number +52 (999) 518 1643 from Monday to Friday from 9:00 a.m. to 5:00 p.m. and Saturday from 9:00 a.m. to 2:00 p.m., also you can contact our 24-hour line at +52 1 999 398 6377.</li>
                        <li>Go Tour acts only as an intermediary between the clients and the individuals and / or morals that provide the hosting services or others for whom the service voucher is issued, therefore Go Tour disclaims all liability for delays, cancellations and overselling of the services due to majeure force or fortuitous events, as well as any loss, accident or irregularity that may occur to its clients and their belongings. Go Tour reserves the right to apply cancellation or reimbursement charges, partial or total, independently of those applied by the service provider.</li>
                    </ol>
                    <br>
                    <p class="text-center">*CHANGES, CANCELLATIONS AND NO SHOW WILL BE SUBJECT TO THE POLICIES OF EACH HOTEL.</p>
                    <p class="text-center">NOTE: This coupon is not valid if it presents blots or without confirmation key.</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>';
// reference the dompdf namespace
use Dompdf\Dompdf;
$baucher = utf8_encode($baucher);

// instantantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->load_html($baucher);

// (optional) setup the paper size and orientation
$dompdf->setPaper('A4');

// change the font
$dompdf->set_option('defaultFont','Courier');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("testst.pdf");
?>