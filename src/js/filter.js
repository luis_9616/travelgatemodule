$(document).ready(function(){
	// let hotels = jQuery.makeArray($("#divBox > .product-item"));
	// console.log(hotels);
	// =======================================
	// AGREGANDO CLASE ACTIVE AL PRIMER ENLACE
	// =======================================
	$('.category_list .category_item[category="all"]').addClass('ct_item-active');

	// ====================
	// FILTRANDO RESULTADOS
	// ====================
	$('.category_item').click(function(){
		var catProduct = $(this).attr('category');
		console.log(catProduct);

		// =============================================
		// AGREGANDO CLASE ACTIVE AL ENLACE SELECCIONADO
		// =============================================
		// $('.category_item').removeClass('ct_item-active');
		// $(this).addClass('ct_item-active');

		// ===================
		// OCULTANDO PRODUCTOS
		// ===================
		$('.product-item').css('transform', 'scale(0)');
		function hideProduct(){
			$('.product-item').hide();
		} setTimeout(hideProduct,400);
		
		// ===================
		// MOSTRANDO PRODUCTOS
		// ===================
		function showProduct(){
			clearInput();
			$('.product-item[category="'+catProduct+'"]').show();
			$('.product-item[category="'+catProduct+'"]').css('transform', 'scale(1)');
		} setTimeout(showProduct,400);
	});

	// =============================
	// MOSTRANDO TODOS LOS PRODUCTOS
	// =============================
	$('.category_item[category="all"]').click(function(){
		function showAll(){
			unchecked();
			$("#star1").prop('checked', false);
			$("#star2").prop('checked', false);
			$("#star3").prop('checked', false);
			$("#star4").prop('checked', false);
			$("#star5").prop('checked', false);
			$('.product-item').show();
			$('.product-item').css('transform', 'scale(1)');
		} setTimeout(showAll,400);
	});

	// =====================================================================
	// funcion que muestra todos los resultados cuando se aplica otro filtro
	// =====================================================================
	function restAll(){
		$('.product-item').show();
		$('.product-item').css('transform', 'scale(1)');
	}
	
	// ================================================
	// funcion que deveria de resetear los radio butons
	// ================================================
	function unchecked(){
		$('input:radio[name=customRadio]').prop('checked',false);
	};
	// $(function(){
	// 	$('#ordenarMinor').on('click', function () {
	// 		var divOrder = $("#divBox > article").sort(function (a, b) {
	// 			return $(a).data("price") > $(b).data("price");
	// 		});
	// 		$(".products-list").html(divOrder);
	// 	});
	// });

	// ==============================================================
	// filtro para mostrar los resultados de mayor a menos o al reves
	// ==============================================================
	$("input[name=customRadio]").click(function () {
		restAll();
		if($('input:radio[name=customRadio]:checked').val() =="optionMajor") {
			var divOrder = $("#divBox > .product-item").sort(function (a, b) {
				return $(a).data("price") > $(b).data("price");
			});
			$(".products-list").html(divOrder);
		} else if($('input:radio[name=customRadio]:checked').val() =="optionMinor"){
			var divOrder = $("#divBox > .product-item").sort(function (a, b) {
				return $(a).data("price") < $(b).data("price");
			});
			$(".products-list").html(divOrder);
		} else{
			$('input:radio[name=customRadio]').attr('checked',false);
		}
	});
	
	// ================================================================
	// filtro para mostrar los resultados segun las estrellas que tenga
	// ================================================================
	$("input[name=star]").click(function () {
		// obtener el valor del input
		let cate = $(this).val();
		console.log(cate);
		
		// ===========================
		// combinacion 1 solo elemento
		// ===========================
		if(($("#star1").prop('checked') == true) && 
		   ($("#star2").prop('checked') == false) && 
		   ($("#star3").prop('checked') == false) && 
		   ($("#star4").prop('checked') == false) && 
		   ($("#star5").prop('checked') == false)) {
			$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();
					showSs('S1');
					showSs('S15');
				} setTimeout(hideProduct,400);
		}
		else if(
		   ($("#star1").prop('checked') == false) && 
		   ($("#star2").prop('checked') == true) && 
		   ($("#star3").prop('checked') == false) && 
		   ($("#star4").prop('checked') == false) && 
		   ($("#star5").prop('checked') == false)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();
					showSs('S2');
					showSs('S25');
				} setTimeout(hideProduct,400);
	 	}
		else if(
		   ($("#star1").prop('checked') == false) && 
		   ($("#star2").prop('checked') == false) && 
		   ($("#star3").prop('checked') == true) && 
		   ($("#star4").prop('checked') == false) && 
		   ($("#star5").prop('checked') == false)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();
					showSs('S3');
					showSs('S35');
				} setTimeout(hideProduct,400);
	 	}
		else if(
		   ($("#star1").prop('checked') == false) && 
		   ($("#star2").prop('checked') == false) && 
		   ($("#star3").prop('checked') == false) && 
		   ($("#star4").prop('checked') == true) && 
		   ($("#star5").prop('checked') == false)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();
					showSs('S4');
					showSs('S45');
				} setTimeout(hideProduct,400);
	 	}
		else if(
		   ($("#star1").prop('checked') == false) && 
		   ($("#star2").prop('checked') == false) && 
		   ($("#star3").prop('checked') == false) && 
		   ($("#star4").prop('checked') == false) && 
		   ($("#star5").prop('checked') == true)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();
					showSs('S5');
					showSs('S55');
				} setTimeout(hideProduct,400);
		}

		// ===================================
		// combinacion usando solo 2 elementos
		// ===================================
		else if(
		   ($("#star1").prop('checked') == true) && 
		   ($("#star2").prop('checked') == true) && 
		   ($("#star3").prop('checked') == false) && 
		   ($("#star4").prop('checked') == false) && 
		   ($("#star5").prop('checked') == false)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();;
					showSs('S1');
					showSs('S15');
					showSs('S2');
					showSs('S25');
				} setTimeout(hideProduct,400);
	 	}
		else if(
		   ($("#star1").prop('checked') == true) && 
		   ($("#star2").prop('checked') == false) && 
		   ($("#star3").prop('checked') == true) && 
		   ($("#star4").prop('checked') == false) && 
		   ($("#star5").prop('checked') == false)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();;
					showSs('S1');
					showSs('S15');
					showSs('S3');
					showSs('S35');
				} setTimeout(hideProduct,400);
	 	}
		else if(
		   ($("#star1").prop('checked') == true) && 
		   ($("#star2").prop('checked') == false) && 
		   ($("#star3").prop('checked') == false) && 
		   ($("#star4").prop('checked') == true) && 
		   ($("#star5").prop('checked') == false)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();;
					showSs('S1');
					showSs('S15');
					showSs('S4');
					showSs('S45');
				} setTimeout(hideProduct,400);
	 	}
		else if(
		   ($("#star1").prop('checked') == true) && 
		   ($("#star2").prop('checked') == false) && 
		   ($("#star3").prop('checked') == false) && 
		   ($("#star4").prop('checked') == false) && 
		   ($("#star5").prop('checked') == true)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();;
					showSs('15');
					showSs('S15');
					showSs('S5');
					showSs('S55');
				} setTimeout(hideProduct,400);
		}		
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == false) && 
			($("#star5").prop('checked') == false)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S2');
					 showSs('S25');
					 showSs('S3');
					 showSs('S35');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == false) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == false)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S2');
					 showSs('S25');
					 showSs('S4');
					 showSs('S45');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == false) && 
			($("#star4").prop('checked') == false) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S2');
					 showSs('S25');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == false) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == false)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S3');
					 showSs('S35');
					 showSs('S4');
					 showSs('S45');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == false) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == false) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S3');
					 showSs('S35');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == false) && 
			($("#star3").prop('checked') == false) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S4');
					 showSs('S45');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}

		// ===================================
		// combinacion usando solo 3 elementos
		// ===================================
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == false) && 
			($("#star5").prop('checked') == false)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S2');
					 showSs('S25');
					 showSs('S3');
					 showSs('S35');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == false) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == false)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S2');
					 showSs('S25');
					 showSs('S4');
					 showSs('S45');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == false) && 
			($("#star4").prop('checked') == false) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S2');
					 showSs('S25');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == false) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == false)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S3');
					 showSs('S35');
					 showSs('S4');
					 showSs('S45');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == false) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == false) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S3');
					 showSs('S35');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == false) && 
			($("#star3").prop('checked') == false) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S4');
					 showSs('S45');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == false)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S2');
					 showSs('S25');
					 showSs('S3');
					 showSs('S35');
					 showSs('S4');
					 showSs('S45');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == false) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S2');
					 showSs('S25');
					 showSs('S3');
					 showSs('S35');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == false) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S2');
					 showSs('S25');
					 showSs('S4');
					 showSs('S45');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == false) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S3');
					 showSs('S35');
					 showSs('S4');
					 showSs('S45');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}

		// ===================================
		// combinacion usando solo 4 elementos
		// ===================================
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == false)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S2');
					 showSs('S25');
					 showSs('S3');
					 showSs('S35');
					 showSs('S4');
					 showSs('S45');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == false) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S2');
					 showSs('S25');
					 showSs('S3');
					 showSs('S35');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == false) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S2');
					 showSs('S25');
					 showSs('S4');
					 showSs('S45');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == false) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S3');
					 showSs('S35');
					 showSs('S4');
					 showSs('S45');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == false) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S2');
					 showSs('S25');
					 showSs('S3');
					 showSs('S35');
					 showSs('S4');
					 showSs('S45');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		else if(
			($("#star1").prop('checked') == true) && 
			($("#star2").prop('checked') == true) && 
			($("#star3").prop('checked') == true) && 
			($("#star4").prop('checked') == true) && 
			($("#star5").prop('checked') == true)) {
				 $('.product-item').css('transform', 'scale(0)');
				 function hideProduct(){
					 $('.product-item').hide();;
					 showSs('S1');
					 showSs('S15');
					 showSs('S2');
					 showSs('S25');
					 showSs('S3');
					 showSs('S35');
					 showSs('S4');
					 showSs('S45');
					 showSs('S5');
					 showSs('S55');
				 } setTimeout(hideProduct,400);
		}
		// =======================
		// final // final // final
		// =======================
		else if(
		   ($("#star1").prop('checked') == false) && 
		   ($("#star2").prop('checked') == false) && 
		   ($("#star3").prop('checked') == false) && 
		   ($("#star4").prop('checked') == false) && 
		   ($("#star5").prop('checked') == false)) {
				$('.product-item').css('transform', 'scale(0)');
				function hideProduct(){
					$('.product-item').hide();
					restAll();
				}setTimeout(hideProduct,400);
	 	}
		else{
				restAll();
		}

		// if($(this).prop('checked')) {
		// 	console.log('checked');
		// 	}else{	
		// 		console.log('unChecked');			
		// }
	});
	
		// ===================
		// MOSTRANDO CATEGORIAS
		// ===================
		function showSs(s){
			$('.product-item[data-cateCode="'+s+'"]').show();
			$('.product-item[data-cateCode="'+s+'"]').css('transform', 'scale(1)');
		} setTimeout(showSs,400);		

	// ==============================================================
	// funcion de busqueda en tiempo real para los nombres de hoteles
	// ==============================================================
	(function ($) {
		$('#filtrar').keyup(function () {
			restAll();
		  var rex = new RegExp($(this).val(), 'i');
		  $('#divBox > .product-item').hide();
		  $('#divBox > .product-item').filter(function () {
			return rex.test($(this).text());
		  }).show();
		})
	  }(jQuery));

	// ==========================================================
	// funcion limpiar input de busqueda para aplicar otro filtro
	// ==========================================================
	function clearInput(){
		$("#filtrar").val("");
	}
	// ==========
	// paginacion
	// ==========
	

    // $('#divBox').pagination({
	// 	dataSource: jQuery.makeArray(hotels),
	// 	// dataSource: hotels,
	// 	// locator: '0',
	// 	pageSize: 5,
    //     callback: function(data, pagination) {
    //         // template method of yourself
    //         var html = template(data);
    //         dataContainer.html(html);
    //     }
    // });
});