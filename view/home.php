<?php
$title = 'Home';
require_once ('../src/vendor/autoload.php');//fluentPDO
require_once ('../model/database.php');//fluentPDO

$controller = 'travelgate';

require_once "../controller/$controller.controller.php";
$controller = ucwords($controller).'controller';
$controller = new $controller;
$controller->Searches();
?>