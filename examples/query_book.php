<?php

require_once __DIR__ . '/../vendor/autoload.php';
include('./config.php');
require_once '../openpaymodulo/vendor/autoload.php'; //OPENPAY
require_once '../openpaymodulo/model/database.php'; //
require_once '../openpaymodulo/model/openpay.php'; //

// use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;

// openpay
$openP = new Openpay1;
    
$cantidad = $_POST["cantidad"]; 
$descrip = $_POST["descrip"]; 
// $correo = $_POST["correo"]; 
$token_id = $_POST['token_id'];
$deviceName = $_POST['deviceName'];
$name = $_POST['fullName'];
$optionRefId = $_POST['optionRefId'];

$clienteper = $_POST['perseuscli'];

$id = 1;
$ape = 'Apellido Uno';
$num = '911';
$amonut = floatval($cantidad);
$status="";
// openpay
try {
  $tmp = $openP->Obtener($id);
  $ide = ($tmp->ide);
  $sk = ($tmp->privateKey);
} catch (\Throwable $th) {
  var_dump($th);
}

$openpay = Openpay::getInstance($ide,$sk);

$customer = array(
  'name'=> $name,
  'last_name'=> $ape,
  'phone_number'=> $num,
  'email'=> 'test@test.com'
);

$chargeRequest = array(
  'method'=>'card',
  'source_id'=> $token_id,
  'amount'=>$amonut,
  'description'=>$descrip,
  'device_session_id'=>$deviceName,//comentado si se va a redirirgir a openpay
  'customer'=>$customer,
  'send_email'=>false,
  'confirm'=> true,//false para redirigir a openpay
  'redirect_url' => 'https://luisuuh.com/');

try {
  $charge = $openpay->charges->create($chargeRequest);
  $title = 'proccess success';

  include('../src/template/header.openpay.php');
?>
<!-- html -->
            <!-- contenedor principal -->
            <div class="row mt-5 col-lg-9 col-md-12 col-sm-12 col-xs-12 m-auto">
                <!-- logo -->
                <img class="mt-1 mb-2" src="../openpaymodulo/assets/img/GOTOUR_LOGO.png" width="100" alt="Logo Gotour" srcset="">
                <!-- card de pagos -->
                    <div class="card-group">
                    <!-- card detalles de pago -->
                        <aside class="card col-lg-4 col-md-12 col-sm-12 col-xs-12 mb-0 p-0">
                            <div class="card-header">
                                <h5 class="card-title">Detalle de pago</h5>
                            </div>
                            <div class="card-body bg-light">
                                <h5 class="card-title">Concepto:</h5>
                                <p class="card-text"><?php echo($charge->description)?></p>
                                <h5 class="card-title">Importe:</h5>
                                <p class="card-text text-monospace text-success" 
                                style="font-size:30px">$<?php echo($charge->amount)?><span style="font-size:15px">EUR</span></p>
                            </div>
                        </aside>
                        <!-- card detalles de la tarjeta -->
                        <form action="./processing.php" method="post" id="form" class="card col-lg-8 col-md-12 col-sm-12 col-xs-12 p-0">
                            <div class="card-header border-0 bg-white">
                                <h5 class="card-title" >El pago se realizo con exito</h5>
                            </div>
                            <div class="card-body"> <!-- body card details -->
                                <section class="p-0 d-flex flex-row flex-wrap">
                                    
                                    <div class="col-lg-7 col-md-12 col-sm-12 p-0">
                                        <h6 class="text-center">Id de transacción: </h6>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 p-0">
                                        <p><?php echo($charge->id)?></p>
                                    </div>

                                    <div class="col-lg-7 col-md-12 col-sm-12 p-0">
                                        <h6 class="text-center">Fecha de pago: </h6>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 p-0">
                                        <p id="date_trans"><?php echo( date('g:ia \o\n l jS F Y', strtotime($charge->creation_date)));?></p>
                                        <p id="date_trans">
                                        <?php 
                                        // echo($charge->creation_date);
                                        ?>
                                        </p>
                                        <p id="date_trans"><?php 
                                        // echo( date('l jS \of F Y \o\n h:i:s A', strtotime($charge->creation_date)));
                                        ?></p>
                                    </div>
                                    
                                    <div class="col-lg-7 col-md-12 col-sm-12 p-0">
                                        <h6 class="text-center">Autorización: </h6>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 p-0">
                                        <p><?php echo($charge->authorization)?></p>
                                    </div>
                                    
                                    <div class="col-lg-7 col-md-12 col-sm-12 p-0">
                                        <h6 class="text-center">Número de tarjeta:</h6>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 p-0">
                                        <p><?php echo($charge->card->card_number)?></p>
                                    </div>
                                </section>
                                <section class="col-12 p-0 mt-4 text-right">
                                <a href="http://yucatanvacations.com.mx/gotour/es" class="btn btn-primary">Continuar</a>
                                </section>
                            </div> <!-- end body card details -->       
                            <div class="card-footer p-0 text-right">
                                <section class="col-12 d-flex flex-row flex-wrap p-0 mt-4 text-center">
                                    <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                        <small class="mb-2">Transacciones realizadas vía:</small> <br>
                                        <img src="../openpaymodulo/assets/img/openpay.png" alt="Openpat logo" srcset="">
                                    </div>
                                </section>
                            </div>
                        </form>
                    </div>
                </div>

<?php
$status=$charge->status;
        } catch (OpenpayApiTransactionError $e) {
            var_dump('ERROR on the transaction: ' . $e->getMessage());
            $status=$e->getMessage();
        } catch (OpenpayApiRequestError $e) {
            var_dump('ERROR on the request: ' . $e->getMessage(), 0);
            $status=$e->getMessage();
        } catch (OpenpayApiConnectionError $e) {
            var_dump('ERROR while connecting to the API: ' . $e->getMessage(), 0);
            $status=$e->getMessage();
        } catch (OpenpayApiAuthError $e) {
            var_dump('ERROR on the authentication: ' . $e->getMessage(), 0);
            $status=$e->getMessage();
        } catch (OpenpayApiError $e) {
            var_dump('ERROR on the API: ' . $e->getMessage(), 0);
            $status=$e->getMessage();
        } catch (Exception $e) {
            var_dump('Error on the script: ' . $e->getMessage(), 0);
            $status=$e->getMessage();
        }

        // var_dump($charge->status);
if ($status=='completed') {
// query book
// query book
// Create Client object to contact the GraphQL endpoint
$lorem = 'mutation book{
  hotelX {
    book(input: {
            optionRefId:"'.$optionRefId.'"
            clientReference: "'.$clienteper.'"
                  deltaPrice: {
              amount: '.$amonut.' 
              percent: 10 
              applyBoth: true 
            },
            paymentCard: {
              cardType: "VI" 
              holder: { 
                name: "Test" 
                surname: "Test" 
              }, number: "0123456789101112" 
              CVC: "123" 
              expire: { 
                month: 8 
                year: 21 
              } 
            },
            holder: { 
              name: "Test"
              surname: "Test" 
            },
            rooms: [ 
              { 
                occupancyRefId: 1, 
                paxes: [
                  {
                    name: "Test1", 
                    surname: "Test1", 
                    age: 23
                  },
                  {
                    name: "Test1", 
                    surname: "Test1", 
                    age: 23
                  }
                ]
              }
            ]
                },
          settings: {
            client: "'.$clientMode.'"
            testMode: true
            context:"'.$context.'"
          }) {
      booking {
        status
        reference{
          client
          hotel
          supplier
        }
        holder{
          name
          surname
        }
        hotel {
          hotelName
          hotelCode
          checkIn
          checkOut
        }
        price {
          currency
          binding
          net
          gross
          
        }
      }
      errors {
        code
        type
        description
      }
      warnings {
        code
        description
      }
    }
  }
}';

// Create the GraphQL query
$gql = <<<QUERY
$lorem
QUERY;

try {
  $results = $client->runRawQuery($gql);
}

catch (QueryError $exception) {
  // Catch query error and desplay error details
  print_r($exception->getErrorDetails());
  // exit;
}
$xd = $results->getData()->hotelX;
// var_dump($xd);
// query book
// query book
?>
<input id="status" type="hidden"  value="<?php echo($xd->book->booking->status)?>">

    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" style="position:absolute;top:1em;right:1em">
      <div class="toast-header">
        <img src="../openpaymodulo/assets/img/GOTOUR_LOGO.png" width="100" class="rounded mr-2" alt="...">
        <strong class="mr-auto">Resumen</strong>
        <small class="text-muted">+</small>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="toast-body">
      Cliente: <br>
  <?php
        echo($xd->book->booking->reference->client);
    ?><br>
  Hotel: <br>
  <?php
        var_dump($xd->book->booking->reference->hotel);
    ?>
  Supplier: <br>
  <?php
        echo($xd->book->booking->reference->supplier);
    ?><br>
  Gross: <br>
  <?php
        echo($xd->book->booking->price->gross);
    ?>
      </div>
    </div>
<script>
$(document).ready(() => {
  var status = $('#status').val();
  if (status === 'OK') {
  $('.toast').toast('show');
  // $('.toast').on('hidden.bs.toast', e => {
    // $(e.currentTarget).remove();
    // console.log('Hide');
  // });
}else{
    alert('algo');
}
});
</script>
<?php    
}else {
  echo('<script> alert("reservation failed");</script>');
}
require_once('../src/template/footer.php');
?>